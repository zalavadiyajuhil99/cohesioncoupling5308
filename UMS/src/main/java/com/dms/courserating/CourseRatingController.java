package com.dms.courserating;

import com.dms.course.Course;
import com.dms.course.CourseDAO;
import com.dms.course.interfaces.ICourseDAO;
import com.dms.courserating.interfaces.ICourseRatingDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;


@Controller
public class CourseRatingController {

    ICourseDAO courseRegistrationDAO = new CourseDAO();
    ICourseRatingDAO courseRatingDAO =new CourseRatingDAO();
    CourseRatingModel courseRating = new CourseRatingModel(courseRatingDAO);

    @RequestMapping(value="/rating",method = RequestMethod.GET)
    public String rating(Model model, HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {

            String userID = (String) session.getAttribute("username");
            ArrayList<Course> coursesList = courseRegistrationDAO.getCourses();
            model.addAttribute("courseList",coursesList);
            return "Rating";
        }
        else{
            return "homepage";
        }

    }

    @RequestMapping(value="/postRating/{courseId}",method = RequestMethod.POST)
    public String postRating(Model model, HttpServletRequest request, @RequestParam("rating") String rating,
                             @PathVariable(name ="courseId") int courseId) throws Exception {
        int rateGiven = Integer.parseInt(rating);
        boolean ratingFlag;
        HttpSession session = request.getSession();

        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            CourseRatingDAO courseRatingDAO = new CourseRatingDAO();
            ratingFlag = courseRating.postCourseRating(courseId,userName,rateGiven);

            if(ratingFlag){
                return "courseRatingSuccessful";
            }
            else{
                return "courseRatingFail";
            }

        }
        else{
            return "homepage";
        }

    }

}
