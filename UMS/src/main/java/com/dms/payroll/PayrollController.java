package com.dms.payroll;

import com.dms.payroll.interfaces.IPayrollModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Controller
public class PayrollController {
    PayrollDAO payRollDAO = new PayrollDAO();
    IPayrollModel payRollModel = new PayrollModel(payRollDAO);
    private final String SALARY_NOT_UPDATED = "Your salary is not updated yet!";

    @RequestMapping(value = "/facultyPayroll", method = RequestMethod.GET)
    public String checkSalaryDetails(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            return "facultySalaryDetails";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/retrieveSalaryInfo", method = RequestMethod.POST)
    public String checkPayroll(Model model, @RequestParam("month") String month, @RequestParam("year") String year, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        int convertedYear = Integer.parseInt(year);
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            Map<Integer, List> salaryList = payRollModel.facultySalaryInfo(userName, month, convertedYear);
            if (salaryList.size() > 0) {
                model.addAttribute("salaryList", salaryList);
                return "facultyPayroll";
            }
            else {
                model.addAttribute("notUpdated",SALARY_NOT_UPDATED);
                return "facultyPayroll";
            }
        }
        return "redirect:/login";
    }
}