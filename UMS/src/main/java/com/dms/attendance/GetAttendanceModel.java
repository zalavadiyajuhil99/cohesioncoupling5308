package com.dms.attendance;

import com.dms.attendance.interfaces.IGetAttendanceDAO;

import java.sql.SQLException;

public class GetAttendanceModel {
    public int retrievingAttendance(String userName, int courseId, IGetAttendanceDAO iGetAttendanceDao) throws SQLException {
        int attendance = iGetAttendanceDao.getAttendance(userName,courseId);
        return attendance;
    }
}
