package com.dms.attendance;

import com.dms.attendance.interfaces.IGetCourseDAO;

import java.sql.SQLException;
import java.util.ArrayList;

public class GetCourseModel {
    public ArrayList<Integer> getCourseID(String userName, IGetCourseDAO iGetCourseDao) throws SQLException {
        ArrayList<Integer> courseId = new ArrayList<>();
        courseId=iGetCourseDao.getCourseID(userName);
        return courseId;
    }
}
