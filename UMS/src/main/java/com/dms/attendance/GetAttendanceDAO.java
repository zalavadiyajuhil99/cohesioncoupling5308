package com.dms.attendance;

import com.dms.attendance.interfaces.IGetAttendanceDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GetAttendanceDAO implements IGetAttendanceDAO {
    private final ISqlConnection connectionManager;

    public GetAttendanceDAO()
    {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public int getAttendance(String userName,int courseId) throws SQLException {
        int daysPresent = -1,totalDays = -1;
        try{
            String query = "select days_present,total_days from CourseRegistrationDetails where studentId = (select studentId from Student where userName="+"'"+userName+"'"+") AND courseId = "+courseId;
            connectionManager.executeRead(query);
            ResultSet result = connectionManager.executeResult(query);

            while(result.next())
            {
                daysPresent = result.getInt(1);
                totalDays = result.getInt(2);
                System.out.println(daysPresent+","+totalDays);
            }
        }catch (SQLException sqlException)
        {
            sqlException.printStackTrace();
        }finally {
            connectionManager.closeConnection();
        }
        AttendancePercentage attendancePercentage = new AttendancePercentage();
        int percentage  = attendancePercentage.calculatePercentage(daysPresent,totalDays);
        return percentage;
    }
}
