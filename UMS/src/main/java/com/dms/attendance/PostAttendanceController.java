package com.dms.attendance;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

@Controller
public class PostAttendanceController {
    @RequestMapping(value = "/postAttendance")
    public String postAttendancePage(ModelMap model, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        PostCourseModel postCourseModel = new PostCourseModel();
        if(session.getAttribute("loggedIn").equals("yes"))
        {
            PostCourseDAO potCourseDao= new PostCourseDAO();
            String username = session.getAttribute("username").toString();
            ArrayList<Integer> courseID = new ArrayList<>();
            courseID = postCourseModel.getCourseIDForFaculty(username,potCourseDao);
            System.out.println(courseID);
            model.addAttribute("courseID",courseID);
            return "postattendance";
        }
        else{
            return "redirect:homepage";
        }
    }

    @PostMapping(value = "/setattendance")
    public String Postattendance(ModelMap model, HttpServletRequest request, @RequestParam("course_ID") int courseId,@RequestParam("student_Id") int studentId,@RequestParam("attendance_status") String attendance_state) throws SQLException {
        HttpSession session = request.getSession();
        PostAttendanceModel postAttendanceModel = new PostAttendanceModel();
        if(session.getAttribute("loggedIn").equals("yes"))
        {
            PostAttendanceDAO postAttendanceDao = new PostAttendanceDAO();
            postAttendanceModel.postAttendance(studentId,courseId,attendance_state, postAttendanceDao);
            return "redirect:postAttendance";
        }
        else{
            return "redirect:homepage";
        }
    }
}
