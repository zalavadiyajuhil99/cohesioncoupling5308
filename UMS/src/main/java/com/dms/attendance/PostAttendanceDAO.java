package com.dms.attendance;

import com.dms.attendance.interfaces.IPostAttendanceDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;


public class PostAttendanceDAO implements IPostAttendanceDAO {
    private final ISqlConnection connectionManager;

    public PostAttendanceDAO()
    {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public void postAttendance(int studentId, int courseId,String attendance_state) throws SQLException {
        int daysPresent = -1,totalDays = -1;
        try{
            String query = "select days_present,total_days from CourseRegistrationDetails where studentId = "+studentId+" AND courseId = "+courseId;
            connectionManager.executeRead(query);
            ResultSet result = connectionManager.executeResult(query);
            while(result.next())
            {
                daysPresent = result.getInt(1);
                totalDays = result.getInt(2);
            }
            if(attendance_state.equals("present"))
            {
                daysPresent++;
                totalDays++;
            }
            else{
                totalDays++;
            }
            String updateQuery = "update CourseRegistrationDetails set days_present= "+daysPresent+",total_days="+totalDays+" where studentId = "+studentId+" AND courseId = "+courseId;
            connectionManager.executeWrite(updateQuery);
        }catch (SQLException se)
        {
            se.printStackTrace();
        }finally {
            connectionManager.closeConnection();
        }
    }
}
