package com.dms.attendance;

import com.dms.attendance.interfaces.IGetCourseDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GetCourseDAO implements IGetCourseDAO {
    private final ISqlConnection connectionManager;

    public GetCourseDAO()
    {
        connectionManager = new SqlConnectionImpl();
    }
    public ArrayList<Integer> getCourseID(String username) throws SQLException{
        ArrayList<Integer> course_id = new ArrayList<>();
        try {
            String query = "select courseId from CourseRegistrationDetails where studentId=(select studentID from Student where userName = " + "'" + username + "')";
            connectionManager.executeRead(query);
            ResultSet result = connectionManager.executeResult(query);

            while (result.next()) {
                course_id.add(result.getInt(1));
            }
        }catch (SQLException se) {

            }finally {
                connectionManager.closeConnection();
            }
        return course_id;
    }
}
