package com.dms.attendance;

public class AttendancePercentage {
    public int calculatePercentage(int daysPresent,int totalDays)
    {
        if(totalDays == 0)
        {
            return -1;
        }
        else
        {
            int percentage = (daysPresent * 100 )/totalDays;
            return percentage;
        }
    }
}
