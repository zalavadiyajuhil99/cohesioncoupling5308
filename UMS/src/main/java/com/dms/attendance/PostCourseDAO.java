package com.dms.attendance;

import com.dms.attendance.interfaces.IPostCourseDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PostCourseDAO implements IPostCourseDAO {
    private final ISqlConnection connectionManager;

    public PostCourseDAO()
    {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public ArrayList<Integer> getCourseIDForFaculty(String username) throws SQLException {
        ArrayList<Integer> course_id = new ArrayList<>();
        try{
            String query = "select courseId from Courses where facultyId=(select FacultyID from Faculty where userName = '"+username+"')";
            connectionManager.executeRead(query);
            ResultSet result = connectionManager.executeResult(query);
            while (result.next())
            {
                course_id.add(result.getInt(1));
            }

        }catch (SQLException se)
        {
            se.printStackTrace();
        }finally {
            connectionManager.closeConnection();
        }
        return course_id;
    }
}
