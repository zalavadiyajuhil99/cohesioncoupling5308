package com.dms.attendance;

import com.dms.attendance.interfaces.IPostAttendanceDAO;

import java.sql.SQLException;

public class PostAttendanceModel {

    public void postAttendance(int studentId, int courseId, String attendance_state, IPostAttendanceDAO iPostAttendanceDao) throws SQLException {
        iPostAttendanceDao.postAttendance(studentId,courseId,attendance_state);
    }
}
