package com.dms.attendance.interfaces;

import java.sql.SQLException;

public interface IPostAttendanceDAO {
    public void postAttendance(int studentId, int courseId,String attendance_state) throws SQLException;
}
