package com.dms.attendance.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IPostCourseDAO {
    public ArrayList<Integer> getCourseIDForFaculty(String username) throws SQLException;
}
