package com.dms.attendance.interfaces;

import java.sql.SQLException;

public interface IGetAttendanceDAO {
    public int getAttendance(String username,int courseId) throws SQLException;
}
