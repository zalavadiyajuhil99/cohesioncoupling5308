package com.dms.attendance.interfaces;

import java.sql.SQLException;
import java.util.ArrayList;

public interface IGetCourseDAO {
    public ArrayList<Integer> getCourseID(String username) throws SQLException;
}
