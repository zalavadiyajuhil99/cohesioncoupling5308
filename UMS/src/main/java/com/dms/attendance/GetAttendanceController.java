package com.dms.attendance;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;

@Controller
public class GetAttendanceController {
    @RequestMapping(value = "/getattendance")
    public String getAttendancePage(ModelMap model, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        GetCourseModel getCourseModel = new GetCourseModel();
        if(session.getAttribute("loggedIn").equals("yes"))
        {
            GetCourseDAO getCourseDao= new GetCourseDAO();
            String userName = session.getAttribute("username").toString();
            ArrayList<Integer> courseID = new ArrayList<>();
            courseID = getCourseModel.getCourseID(userName,getCourseDao);
            System.out.println(courseID);
            model.addAttribute("courseID",courseID);
            return "getattendance";
        }
        else{
            return "homepage";
        }
    }

    @PostMapping(value = "/retrieveattendance")
    public String retrievingAttendance(ModelMap model, HttpServletRequest request, @RequestParam("course_ID") int courseId, RedirectAttributes redirectAttributes) throws SQLException {
        GetAttendanceModel getAttendanceModel = new GetAttendanceModel();
        GetAttendanceDAO getAttendanceDao = new GetAttendanceDAO();
        HttpSession session = request.getSession();
        String userName = session.getAttribute("username").toString();
        int attendance = getAttendanceModel.retrievingAttendance(userName,courseId,getAttendanceDao);
        if(attendance < 0)
        {
            redirectAttributes.addFlashAttribute("attendance","classes did not start yet");
        }
        else
        {
            System.out.println(attendance);
            redirectAttributes.addFlashAttribute("attendance",attendance);
        }

        return "redirect:getattendance";
    }
}
