package com.dms.attendance;

import com.dms.attendance.interfaces.IPostCourseDAO;

import java.sql.SQLException;
import java.util.ArrayList;

public class PostCourseModel {

    public ArrayList<Integer> getCourseIDForFaculty(String userName, IPostCourseDAO iPostCourseDao) throws SQLException{
        ArrayList<Integer> courseId = new ArrayList<>();
        courseId=iPostCourseDao.getCourseIDForFaculty(userName);
        return courseId;
    }
}
