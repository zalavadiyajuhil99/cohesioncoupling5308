package com.dms.updateinformation;

import com.dms.updateinformation.interfaces.IUpdateInformationDAO;

import java.sql.SQLException;

public class UpdateInformationModel {
    public void updateInformation(String firstName, String lastName, long dob, String email, String password, String username, IUpdateInformationDAO iUpdateInformationDao) throws SQLException{
        iUpdateInformationDao.updateInfo(firstName,lastName,dob,email,password,username);
    }
}
