package com.dms.updateinformation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.dms.authentication.ForgotPasswordVerify;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class UpdateInformationFacutlyController {
    @RequestMapping(value="/updateInformationFaculty")
    public String showLoginPage(ModelMap model, HttpServletRequest request){
        HttpSession session = request.getSession();
        if(session.getAttribute("loggedIn").equals("yes"))
        {
            return "updateInformationFaculty";
        }
        else
            return "homepage";
    }

    @PostMapping("/updateFacultyInformation")
    public String updateInfo(ModelMap model, HttpServletRequest request, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("dateOfBirth") String dob, @RequestParam("emailId") String email, @RequestParam("password") String password, @RequestParam("reenterPassword") String reenterPassword) throws Exception {
        UpdateInformationDAO userDao= new UpdateInformationDAO();
        UpdateInformationModel updateInformationModel = new UpdateInformationModel();
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        ForgotPasswordVerify forgotPasswordVerify = new ForgotPasswordVerify();
        Date dateOfBirth = new SimpleDateFormat("dd-MM-yyyy").parse(dob);
        if(forgotPasswordVerify.passwordVerify(password,reenterPassword)){
            updateInformationModel.updateInformation(firstName,lastName,dateOfBirth.getTime(),email,password,username,userDao);
            return "redirect:updateInformationFaculty";
        }
        else
        {
            return "redirect:facultylogin";
        }

    }
}
