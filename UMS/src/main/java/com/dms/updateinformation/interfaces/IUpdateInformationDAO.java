package com.dms.updateinformation.interfaces;

import java.sql.SQLException;

public interface IUpdateInformationDAO {
    void updateInfo(String firstName, String lastName, long dob, String email, String password, String username) throws SQLException;
}
