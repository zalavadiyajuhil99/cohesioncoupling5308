package com.dms.updateinformation;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.dms.authentication.ForgotPasswordVerify;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;

@Controller
public class UpdateInformationStudentController {
    @RequestMapping(value="/updateInformationStudent")
    public String showLoginPage(HttpServletRequest request){
        HttpSession session = request.getSession();
        if(session.getAttribute("loggedIn").equals("yes"))
        {
            return "updateInformationStudent";
        }
        else
            return "homepage";
    }

    @PostMapping("/updateinformation")
    public String updateInfo(ModelMap model, HttpServletRequest request, @RequestParam("firstName") String firstName, @RequestParam("lastName") String lastName, @RequestParam("dateOfBirth") String dob, @RequestParam("email") String email, @RequestParam("password") String password, @RequestParam("reenterPassword") String reenterPassword) throws Exception {
        UpdateInformationDAO userDao= new UpdateInformationDAO();
        UpdateInformationModel updateInformationModel = new UpdateInformationModel();
        HttpSession session = request.getSession();
        String username = session.getAttribute("username").toString();
        java.util.Date dateOfBirth = new SimpleDateFormat("dd-MM-yyyy").parse(dob);
        ForgotPasswordVerify forgotPasswordVerify = new ForgotPasswordVerify();
        if(forgotPasswordVerify.passwordVerify(password,reenterPassword)){
            updateInformationModel.updateInformation(firstName,lastName,dateOfBirth.getTime(),email,password,username,userDao);
            return "redirect:updateInformationStudent";
        }
        else
        {
            return "redirect:studentlogin";
        }

    }
}
