package com.dms.updateinformation;

import com.dms.updateinformation.interfaces.IUpdateInformationDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.*;

public class UpdateInformationDAO implements IUpdateInformationDAO {
    private final ISqlConnection connectionManager;

    public UpdateInformationDAO() { connectionManager = new SqlConnectionImpl();
    }

    public void updateInfo(String firstName, String lastName, long dob, String email, String password, String username) throws SQLException {
        try {
            String updateQuery = "update User set firstName = '" + firstName + "' , lastName = '" + lastName + "', dob = '" + dob + "' ,email = '" + email + "' , password = '" + password + "' where username='" + username + "'";
            System.out.println(updateQuery);
            connectionManager.executeWrite(updateQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }

    }
}
