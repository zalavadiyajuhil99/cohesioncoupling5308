package com.dms.timetable;

import com.dms.timetable.interfaces.ITimetableStudentDAO;

import java.sql.SQLException;
import java.util.HashMap;

public class TimeTableStudentModel {
    public HashMap<Integer,String> getTimetable(String userName, ITimetableStudentDAO iTimetableStudentDao) throws SQLException {
        HashMap<Integer,String> timetable = iTimetableStudentDao.getTimetable(userName);
        return timetable;
    }
}
