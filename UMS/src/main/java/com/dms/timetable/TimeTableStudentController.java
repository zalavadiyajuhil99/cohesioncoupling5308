package com.dms.timetable;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.HashMap;

@Controller
public class TimeTableStudentController {
        @RequestMapping(value = "/timetableStudent")
        public String getTimeTable(ModelMap model, HttpServletRequest request) throws SQLException {
            HttpSession session = request.getSession();
            TimeTableStudentModel timeTableStudentModel = new TimeTableStudentModel();
            TimetableStudentDAO timetableStudentDao = new TimetableStudentDAO();
            if (session.getAttribute("loggedIn").equals("yes")) {
                String userName = session.getAttribute("username").toString();
                HashMap<Integer,String> timetable = timeTableStudentModel.getTimetable(userName, timetableStudentDao);
                System.out.println(timetable);
                model.addAttribute("timetable",timetable);
                return "timetableStudent";
            } else {
                return "homepage";
            }
        }
}
