package com.dms.timetable;

import com.dms.timetable.interfaces.ITimeTableFacultyDAO;

import java.sql.SQLException;
import java.util.HashMap;

public class TimeTableFacultyModel {
    public HashMap<Integer,String> getTimetable(String userName, ITimeTableFacultyDAO iTimeTableFacultyDao) throws SQLException {
        HashMap<Integer,String> timetable = iTimeTableFacultyDao.getTimetable(userName);
        return timetable;
    }
}
