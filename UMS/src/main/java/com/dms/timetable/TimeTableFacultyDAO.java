package com.dms.timetable;

import com.dms.timetable.interfaces.ITimeTableFacultyDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class TimeTableFacultyDAO implements ITimeTableFacultyDAO {
    private final ISqlConnection connectionManager;

    public TimeTableFacultyDAO()
    {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public HashMap<Integer, String> getTimetable(String username) throws SQLException {
        HashMap<Integer,String> timetable = new HashMap<Integer, String>();
        try{
            String query = "select timetable, courseId from Courses where facultyId IN (select FacultyId from Faculty where userName='"+username+"');";
            connectionManager.executeRead(query);
            ResultSet result = connectionManager.executeResult(query);
            while(result.next())
            {
                System.out.println(result.getString(1));
                timetable.put(result.getInt(2),result.getString(1));
            }
        }catch (SQLException sqlException)
        {
            sqlException.printStackTrace();
        }finally {
            connectionManager.closeConnection();
        }
        return timetable;
    }
}
