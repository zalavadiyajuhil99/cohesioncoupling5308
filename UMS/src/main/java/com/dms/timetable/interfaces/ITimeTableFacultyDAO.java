package com.dms.timetable.interfaces;

import java.sql.SQLException;
import java.util.HashMap;

public interface ITimeTableFacultyDAO {
    public HashMap<Integer,String> getTimetable(String username) throws SQLException;
}
