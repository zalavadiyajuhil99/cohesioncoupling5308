package com.dms.timetable.interfaces;

import java.sql.SQLException;
import java.util.HashMap;

public interface ITimetableStudentDAO {
    HashMap<Integer,String> getTimetable(String username) throws SQLException;
}
