package com.dms.library;

import com.dms.library.interfaces.IBook;
import com.dms.library.interfaces.IBookOperation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Book implements IBook {
    private static final String DATE_ERROR = "Return Date should be grater than issue Date";
    private static final String DATE_PATTERN = "dd-MM-yyyy";
    IBookOperation bookOperation = new BookOperationDAO();

    private int bookId;
    private String bookName;
    private String bookAuthor;
    private int bookQuantity;
    private String issueDate;
    private String returnDate;

    public Book() {

    }

    public Book(String bookName) {
        this.bookName = bookName;
    }

    public Book(String bookName, String bookAuthor) {
        this.bookName = bookName;
        this.bookAuthor = bookAuthor;
    }

    public Book(int bookId, String bookName, String issueDate, String returnDate) {
        this.bookId = bookId;
        this.bookName = bookName;
        this.issueDate = issueDate;
        this.returnDate = returnDate;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getBookQuantity() {
        return bookQuantity;
    }

    public void setBookQuantity(int bookQuantity) {
        this.bookQuantity = bookQuantity;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(String returnDate) {
        this.returnDate = returnDate;
    }

    public boolean requestBook(String username, int bookID, long dateToBeChecked, long returnDate) {
        try {
            if (bookOperation.getAvailableBookCount(bookID) > 0 && bookOperation.checkBookAvailability(bookID, dateToBeChecked, returnDate)) {
                    bookOperation.reduceBookCount(bookID);
                    bookOperation.issueBook(username, bookID, dateToBeChecked, returnDate);
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public Boolean validateDatesForBookRequest(String issueDate, String returnDate) {
        Date checkIssueDate;
        Date checkReturnDate;
        try {
            checkIssueDate = new SimpleDateFormat(DATE_PATTERN).parse(issueDate);
            checkReturnDate = new SimpleDateFormat(DATE_PATTERN).parse(returnDate);
            if(checkReturnDate.getTime() < checkIssueDate.getTime()) {
                throw new IllegalArgumentException(DATE_ERROR);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }
}
