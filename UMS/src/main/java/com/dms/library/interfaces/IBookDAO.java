package com.dms.library.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface IBookDAO {
    List<IBook> getBookList() throws SQLException;

    Boolean addBook(String bookName, String bookAuthor, int bookQuantity) throws SQLException;

    String removeBook(String bookName) throws SQLException;

    List<IBook> searchBook(String text) throws SQLException;
}
