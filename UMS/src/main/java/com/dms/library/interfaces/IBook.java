package com.dms.library.interfaces;

public interface IBook {
    String getBookName();

    void setBookName(String bookName);

    String getBookAuthor();

    void setBookAuthor(String bookAuthor);

    int getBookId();

    void setBookId(int bookId);

    int getBookQuantity();

    void setBookQuantity(int bookQuantity);

    String getIssueDate();

    void setIssueDate(String issueDate);

    String getReturnDate();

    void setReturnDate(String returnDate);

    Boolean validateDatesForBookRequest(String issueDate, String returnDate);

    boolean requestBook(String username, int bookID, long dateToBeChecked, long returnDate);
}
