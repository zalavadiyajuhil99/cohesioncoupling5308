package com.dms.library;

import com.dms.library.interfaces.IBook;
import com.dms.library.interfaces.IBookOperation;
import com.dms.notification.interfaces.ISendEmail;
import com.dms.notification.SendEmail;
import com.dms.user.interfaces.IUserDAO;
import com.dms.user.UserDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BookOperationDAO implements IBookOperation {
    private static final String DATE_PATTERN = "dd-MM-yyyy";
    private static final String BOOK_ID_COLUMN = "bookId";
    private static final String BOOK_NAME_COLUMN = "bookName";
    private static final String ISSUE_DATE_COLUMN = "issueDate";
    private static final String RETURN_DATE_COLUMN = "returnDate";
    private static final String AVAILABLE_BOOK_COUNT_COLUMN = "availableBookCount";

    private final ISqlConnection connectionManager;
    IUserDAO userDAO = new UserDAO();

    public BookOperationDAO() {
        connectionManager = new SqlConnectionImpl();
    }

    public List<IBook> getIssuedBookListForUser(String username) throws SQLException {
        List<IBook> issuedBookList = new ArrayList<>();
        try {
            DateFormat formatDate = new SimpleDateFormat(DATE_PATTERN);
            int userId = userDAO.getUserId(username);
            String selectIssuedBooksQuery = "SELECT Book.bookName, BorrowBook.bookId,BorrowBook.issueDate,BorrowBook.returnDate FROM ((Book INNER JOIN BorrowBook ON Book.bookId = BorrowBook.bookId)) where BorrowBook.userId ='" + userId + "'";
            connectionManager.executeRead(selectIssuedBooksQuery);
            ResultSet result = connectionManager.executeResult(selectIssuedBooksQuery);
            while (result.next()) {
                IBook book = new Book();
                Date issueDate = new Date(result.getLong(ISSUE_DATE_COLUMN));
                Date returnDate = new Date(result.getLong(RETURN_DATE_COLUMN));
                book.setBookId(result.getInt(BOOK_ID_COLUMN));
                book.setBookName(result.getString(BOOK_NAME_COLUMN));
                book.setIssueDate(formatDate.format(issueDate));
                book.setReturnDate(formatDate.format(returnDate));
                issuedBookList.add(book);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return issuedBookList;
    }

    public boolean checkBookAvailability(int bookID, long dateToBeChecked, long returnDate) throws SQLException {
        boolean availability = true;
        try {
            String countQuery = "SELECT * FROM BorrowBook WHERE bookId = " + bookID;
            connectionManager.executeRead(countQuery);
            ResultSet resultSet = connectionManager.executeResult(countQuery);
            while (resultSet.next()) {
                long issueDate = resultSet.getLong(ISSUE_DATE_COLUMN);
                long bookReturn = resultSet.getLong(RETURN_DATE_COLUMN);
                if ((dateToBeChecked > issueDate && dateToBeChecked < bookReturn) || (returnDate > issueDate && returnDate < bookReturn)) {
                    availability = false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return availability;
    }

    public Boolean issueBook(String username, int bookId, long issueDate, long returnDate) throws SQLException {
        try {
            int userId = userDAO.getUserId(username);
            String requestQuery = "INSERT into BorrowBook(userId,bookId,issueDate,returnDate)  VALUES('" + userId + "','" + bookId + "','" + issueDate + "','" + returnDate + "')";
            connectionManager.executeWrite(requestQuery);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return true;
    }

    public int getAvailableBookCount(int bookID) throws SQLException {
        try {
            String countQuery = "SELECT availableBookCount FROM Book WHERE bookId = " + bookID;
            connectionManager.executeRead(countQuery);
            ResultSet resultSet = connectionManager.executeResult(countQuery);
            if (resultSet.next()) {
                return resultSet.getInt(AVAILABLE_BOOK_COUNT_COLUMN);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return 0;
    }

    public boolean returnBook(int bookID) throws SQLException {
        try {
            String returnBookQuery = "Delete from BorrowBook  WHERE bookId = " + bookID;
            connectionManager.executeWrite(returnBookQuery);
            addBookCount(bookID);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return false;
    }

    public boolean addBookCount(int bookID) throws SQLException {
        try {
            String requestQuery = "UPDATE Book SET availableBookCount=availableBookCount+1 WHERE bookId = '" + bookID + "'";
            connectionManager.executeWrite(requestQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return true;
    }

    public boolean reduceBookCount(int bookID) throws SQLException {
        try {
            String requestQuery = "UPDATE Book SET availableBookCount=availableBookCount-1 WHERE bookId = '" + bookID + "'";
            connectionManager.executeWrite(requestQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return true;
    }

    public boolean isBookAlreadyIssued(String username, int bookId) throws SQLException {
        try {
            int userId = userDAO.getUserId(username);
            String selectQuery = "SELECT bookId FROM BorrowBook where userId = '" + userId + "'";
            connectionManager.executeRead(selectQuery);
            ResultSet resultSet = connectionManager.executeResult(selectQuery);
            while (resultSet.next()) {
                if (resultSet.getInt(1) == bookId) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return false;
    }

    public boolean bookNotificationRequest(String username, int bookId) throws SQLException {
        try {
            int userId = userDAO.getUserId(username);
            String insertQuery = "INSERT INTO BookNotification(userId, bookId) VALUES('" + userId + "','" + bookId + "')";
            connectionManager.executeWrite(insertQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return true;
    }

    public boolean notifyUsersForBookAvailability(int bookId) throws SQLException {
        final String mailSubject = "Notification for Book";
        final String mailBody = "Hi there ! Book with bookId :" + bookId + " is now available in the library";
        try {
            int bookCount = getAvailableBookCount(bookId);
            if (bookCount > 0) {
                String selectQuery = "Select userId from BookNotification where bookId = '" + bookId + "'";
                connectionManager.executeRead(selectQuery);
                ResultSet resultSet = connectionManager.executeResult(selectQuery);
                while (resultSet.next()) {
                    ISendEmail sendEmailNotification = new SendEmail();
                    int userId = resultSet.getInt("userId");

                    String userMailId = userDAO.getUserEmail(userId);
                    String[] mailID = {userMailId};
                    sendEmailNotification.sendMailNotification(mailID, mailSubject, mailBody);

                    this.deleteNotifyRequestFromBookNotification(userId);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return true;
    }

    public boolean deleteNotifyRequestFromBookNotification(int userId) throws SQLException {
        try {
            String deleteQuery = "Delete from BookNotification  where userId = " + userId + "";
            connectionManager.executeWrite(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return true;
    }
}
