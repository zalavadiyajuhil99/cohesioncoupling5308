package com.dms.library;

import com.dms.library.interfaces.IBook;
import com.dms.library.interfaces.IBookDAO;
import com.dms.library.interfaces.IBookOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Controller
public class BookController {
    private static final String LIBRARY_PAGE = "library";
    private static final String BOOKS_PAGE = "addBook";
    private static final String REDIRECT_BOOKS_PAGE = "redirect:/addBook";
    private static final String NOTIFY_PAGE = "notifyWhenBookAvailable";
    private static final String CHECK_BOOK_AVAILABILITY_PAGE = "checkBookAvailability";
    private static final String ALREADY_ISSUED_BOOK_PAGE = "bookAlreadyIssued";
    private static final String ERROR_PAGE = "error";
    private static final String BOOK_AVAILABLE_PAGE = "bookAvailableSuccess";
    private static final String BOOK_NOT_AVAILABLE_PAGE = "bookNotAvailable";
    private static final String ISSUED_BOOKS_PAGE = "showIssuedBooks";
    private static final String REDIRECT_ISSUED_BOOKS_PAGE = "redirect:/showIssuedBooks";
    private static final String BOOK_RETURN_FAILED_PAGE = "bookReturnError";
    private static final String DATE_PATTERN = "dd-MM-yyyy";
    private static final String USER_LOGGED_IN = "loggedIn";
    private static final String USER_NAME = "username";
    private static final String YES = "yes";

    IBookDAO bookDAO = new BookDAO();
    IBookOperation bookOperation = new BookOperationDAO();

    @RequestMapping(value = "/addBook", method = RequestMethod.GET)
    public ModelAndView bookList(HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        if (StringUtils.equals(loggedIn, YES)) {
            List<IBook> bookList = bookDAO.getBookList();
            ModelAndView model = new ModelAndView(BOOKS_PAGE);
            model.addObject("list", bookList);
            return model;
        }
        return new ModelAndView(ERROR_PAGE);
    }

    @RequestMapping(value = "/addBook")
    public String showBook() {
        return BOOKS_PAGE;
    }

    @RequestMapping("/ProcessAddBook")
    public String addBook(@RequestParam("bookName") String bookName, @RequestParam("bookAuthor") String bookAuthor, @RequestParam("bookQuantity") int bookQuantity) throws SQLException {
        bookDAO.addBook(bookName, bookAuthor, bookQuantity);
        return REDIRECT_BOOKS_PAGE;
    }

    @RequestMapping(value = "/{bookName}/removeBook", method = RequestMethod.GET)
    public ModelAndView removeBook(@PathVariable String bookName) throws SQLException {
        bookDAO.removeBook(bookName);
        return new ModelAndView(REDIRECT_BOOKS_PAGE);
    }

    @RequestMapping(value = "/library")
    public String showLibrary() {
        return LIBRARY_PAGE;
    }

    @RequestMapping(value = "/searchBook", method = RequestMethod.GET)
    public ModelAndView searchBook(@RequestParam("searchKeyword") String text) throws SQLException {
        List<IBook> bookList = bookDAO.searchBook(text);
        ModelAndView model = new ModelAndView(LIBRARY_PAGE);
        model.addObject("list", bookList);
        return model;
    }

    @RequestMapping(value = "/notifyBookWhenAvailable", method = RequestMethod.GET)
    public ModelAndView notifyBook(@RequestParam("bookId") int bookId, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        if (StringUtils.equals(loggedIn, YES)) {
            String userName = (String) session.getAttribute("username");
            ModelAndView model = new ModelAndView(NOTIFY_PAGE);
            bookOperation.bookNotificationRequest(userName, bookId);
            return model;
        }
        return new ModelAndView(ERROR_PAGE);
    }

    @RequestMapping(value = "/library", method = RequestMethod.GET)
    public ModelAndView bookListForLibrary(HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        if (StringUtils.equals(loggedIn, YES)) {
            List<IBook> bookList = bookDAO.getBookList();
            ModelAndView model = new ModelAndView(LIBRARY_PAGE);
            model.addObject("list", bookList);
            return model;
        }
        return new ModelAndView(ERROR_PAGE);
    }

    @RequestMapping(value = "/checkBookAvailability/{bookId}", method = RequestMethod.GET)
    public String showBookAvailability(@PathVariable(name = "bookId") int bookId, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        if (StringUtils.equals(loggedIn, YES)) {
            String userName = (String) session.getAttribute("username");
            boolean bookAlreadyIssued = bookOperation.isBookAlreadyIssued(userName, bookId);
            ModelAndView model = new ModelAndView(CHECK_BOOK_AVAILABILITY_PAGE);
            model.addObject("bookId", bookId);
            if (bookAlreadyIssued) {
                return ALREADY_ISSUED_BOOK_PAGE;
            } else {
                return CHECK_BOOK_AVAILABILITY_PAGE;
            }
        }
        return ERROR_PAGE;
    }

    @RequestMapping(value = "/checkBookAvailability/checkBook", method = RequestMethod.GET)
    public ModelAndView requestBook(@RequestParam("bookId") int bookId, @RequestParam("issueDate") String issueDate, @RequestParam("returnDate") String returnDate, HttpServletRequest request) throws ParseException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        IBook book = new Book();
        Date issueDateCheck = new SimpleDateFormat(DATE_PATTERN).parse(issueDate);
        Date returnDateCheck = new SimpleDateFormat(DATE_PATTERN).parse(returnDate);
        if (StringUtils.equals(loggedIn, YES) && book.validateDatesForBookRequest(issueDate, returnDate)) {
            String userName = (String) session.getAttribute("username");
            if (book.requestBook(userName, bookId, issueDateCheck.getTime(), returnDateCheck.getTime())) {
                return new ModelAndView(BOOK_AVAILABLE_PAGE);
            }
        }
        return new ModelAndView(BOOK_NOT_AVAILABLE_PAGE);
    }

    @RequestMapping(value = "/showIssuedBooks", method = RequestMethod.GET)
    public ModelAndView issuedBookList(HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        ModelAndView model = new ModelAndView(ISSUED_BOOKS_PAGE);
        if (StringUtils.equals(loggedIn, YES)) {
            String userName = (String) session.getAttribute(USER_NAME);
            List<IBook> issuedBookList = bookOperation.getIssuedBookListForUser(userName);
            model.addObject("issuedBookList", issuedBookList);
        }
        return model;
    }

    @RequestMapping(value = "/returnBook")
    public String returnBook(@RequestParam("bookId") int bookId) throws SQLException {
        if (bookOperation.returnBook(bookId)) {
            BookOperationDAO bookOperationDAO = new BookOperationDAO();
            bookOperationDAO.notifyUsersForBookAvailability(bookId);
            return REDIRECT_ISSUED_BOOKS_PAGE;
        } else return BOOK_RETURN_FAILED_PAGE;
    }
}
