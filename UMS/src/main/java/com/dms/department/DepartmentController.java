package com.dms.department;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;
import java.util.ArrayList;

@Controller
public class DepartmentController {

    @RequestMapping(value="departments")
    public String showDepartmentPage(ModelMap model) throws SQLException {
        DepartmentDAO ddao = new DepartmentDAO();
        ArrayList<String> departmentnames = new ArrayList<>();
        departmentnames = ddao.getDepartmentName();
        model.addAttribute("departmentnames",departmentnames);
        System.out.println(departmentnames);
        return "departments";
    }
}
