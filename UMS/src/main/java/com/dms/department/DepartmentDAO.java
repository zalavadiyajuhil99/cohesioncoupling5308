package com.dms.department;

import connection.sql.SqlConnectionImpl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class DepartmentDAO {
    Connection con = null;
    Statement statement = null;

    public DepartmentDAO(){
        con = new SqlConnectionImpl().openConnection();
    }

    public void insert() throws SQLException {
        String query="insert into departments values()";
        statement.execute(query);
    }
    public ArrayList<String> getDepartmentName() throws SQLException {
        statement = con.createStatement();
        String query = "select department_name from departments";
        ArrayList<String> dept_name = new ArrayList<>();
        ResultSet rs = statement.executeQuery(query);
        while(rs.next())
        {
            dept_name.add(rs.getString(1));
        }
        rs.close();
        con.close();
        return dept_name;
    }
}
