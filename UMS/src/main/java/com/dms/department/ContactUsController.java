package com.dms.department;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.SQLException;


@Controller
public class ContactUsController {

    //StudentRepo studentrepo;
    @RequestMapping(value="contactus")
    public String showLoginPage(ModelMap model) throws SQLException {
        return "contactus";
    }
}
