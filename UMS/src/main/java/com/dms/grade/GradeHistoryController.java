package com.dms.grade;

import com.dms.grade.interfaces.IGradeHistoryModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


@Controller
public class GradeHistoryController {
    GradeHistoryDAO gradeHistoryDAO = new GradeHistoryDAO();
    IGradeHistoryModel iGradeHistoryModel = new GradeHistoryModel(gradeHistoryDAO);

    @RequestMapping(value = "/showGrades", method = RequestMethod.GET)
    public String selectCourse(ModelMap model, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            List<String> studentCoursesList = iGradeHistoryModel.courseNamesList(userName);
            model.addAttribute("studentCoursesList", studentCoursesList);
            return "displayCourses";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/retrieveGrade", method = RequestMethod.POST)
    public String showGradeHistoryPage(ModelMap model, HttpServletRequest request, @RequestParam("selectedCourse") String selectedCourse) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            Map<Integer,String> grades = iGradeHistoryModel.calculateGrades(userName,selectedCourse);
            model.addAttribute("Studentgrades",grades);
            return "gradeHistory";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/getCgpa", method = RequestMethod.POST)
    public String displayCgpa(ModelMap model, HttpServletRequest request, @RequestParam("semester") int semester) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            double cgpa = iGradeHistoryModel.calculateCgpa(userName,semester);
            model.addAttribute("cgpa",cgpa);
            return "displayCGPA";
        }
        return "redirect:/login";
    }
}

