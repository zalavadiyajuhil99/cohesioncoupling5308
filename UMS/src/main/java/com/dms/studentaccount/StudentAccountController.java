package com.dms.studentaccount;

import com.dms.studentaccount.interfaces.IStudentAccountDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class StudentAccountController {

    IStudentAccountDAO studentAccountDAO = new StudentAccountDAO();
    StudentAccountModel studentAccountModel;

    @RequestMapping(value="/studentAccount",method = RequestMethod.GET)
    public String showStudentAccountPage(Model model, HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            System.out.println("Getting Session");
            String userName = (String) session.getAttribute("username");
            StudentAccountDAO studentAccountDAO = new StudentAccountDAO() ;
            studentAccountModel = studentAccountDAO.getStudentAccountDetails(userName);
            model.addAttribute("studentAccount",studentAccountModel);
            return "studentAccount";
        }
        else{
            return "homepage";
        }

    }
}
