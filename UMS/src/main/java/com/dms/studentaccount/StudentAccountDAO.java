package com.dms.studentaccount;

import com.dms.studentaccount.interfaces.IStudentAccountDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;


public class StudentAccountDAO  implements IStudentAccountDAO {
    private final ISqlConnection connectionManager;
    private static final int COURSE_FEES = 3000;
    public StudentAccountDAO() {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public StudentAccountModel getStudentAccountDetails(String userName) throws SQLException {

        StudentAccountModel studentAccount = new StudentAccountModel();
        int studentId = 0;
        try {
            String studentIdQuery = "SELECT studentID FROM Student where userName = '" + userName + "'";
            connectionManager.executeRead(studentIdQuery);
            ResultSet resultSet = connectionManager.executeResult(studentIdQuery);

            while (resultSet.next()) {
                studentId = resultSet.getInt("studentID");
            }

            String getStudentAccountDetailQuery = "SELECT * FROM StudentAccount WHERE studentId =" + studentId ;
            connectionManager.executeRead(getStudentAccountDetailQuery);
            resultSet = connectionManager.executeResult(getStudentAccountDetailQuery);
            while (resultSet.next()) {
                studentAccount.setStudentId(resultSet.getInt("studentId"));
                studentAccount.setBilledAmount(resultSet.getInt("billedAmount"));
                studentAccount.setUnbilledAmount(resultSet.getInt("unbilledAmount"));
                studentAccount.setTotalAmount(resultSet.getInt("totalAmount"));

            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            connectionManager.closeConnection();
        }
        return  studentAccount;
    }

    @Override
    public void deductMoney(int studentId) throws SQLException{
        try {
            String deductAmountFromAccount = "UPDATE StudentAccount SET billedAmount = billedAmount +" + COURSE_FEES + ", unbilledAmount = unbilledAmount - " + COURSE_FEES + " where studentId =" + studentId;
            connectionManager.executeWrite(deductAmountFromAccount);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            connectionManager.closeConnectionWithoutResultset();
        }
    };
    @Override
    public void depositMoney(int studentId) throws SQLException{
        try {
            String depositAmountIntoAccount = "UPDATE StudentAccount SET billedAmount = billedAmount -" + COURSE_FEES + ", unbilledAmount = unbilledAmount + " + COURSE_FEES + " where studentId =" + studentId;
            connectionManager.executeWrite(depositAmountIntoAccount);
        }
         catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            connectionManager.closeConnectionWithoutResultset();
        }
    }
}
