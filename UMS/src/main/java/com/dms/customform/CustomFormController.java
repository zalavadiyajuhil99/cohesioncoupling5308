package com.dms.customform;

import net.bytebuddy.implementation.bind.MethodDelegationBinder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class CustomFormController {
    QuestionFormDAO questionFormDAO = new QuestionFormDAO();

    @RequestMapping(value = "/customForm")
    public String showCustomForm() {
        return "customForm";
    }

    @RequestMapping(value = "/processCustomForm", method = RequestMethod.POST)
    public ModelAndView processQuestionForm(@RequestParam("formName") String formName, HttpServletRequest request) throws SQLException {
        Map<String, String[]> parameterMap = request.getParameterMap();

        FormModel form = new FormModel();
        form.setFormName(parameterMap.get("formName")[0]);
        form.setQuestions(new ArrayList<>());
        int noOfQuestions = Integer.parseInt(parameterMap.get("q_no")[0]);
        for (int i = 1; i <= noOfQuestions; i++) {
            QuestionModel questionModel = new QuestionModel();
            questionModel.setQuestionType(parameterMap.get("q_type_" + i)[0]);
            questionModel.setQuestionDescription(parameterMap.get("q_desc_" + i)[0]);
            if (questionModel.getQuestionType().equals("2") || questionModel.getQuestionType().equals("4")) {
                questionModel.setOptionValue1(parameterMap.get("q_desc_" + i + "_detail_value_1")[0]);
                questionModel.setOptionValue2(parameterMap.get("q_desc_" + i + "_detail_value_2")[0]);
                questionModel.setOptionValue3(parameterMap.get("q_desc_" + i + "_detail_value_3")[0]);
                questionModel.setOptionValue4(parameterMap.get("q_desc_" + i + "_detail_value_4")[0]);
            }
            form.getQuestions().add(questionModel);
        }
        questionFormDAO.insertFormData(form);
        List<QuestionModel> questionList = questionFormDAO.getQuestionListForFaculty(formName);
        ModelAndView model = new ModelAndView("addCustomFormSuccess");
        model.addObject("list",questionList);
        return model;
    }

    @RequestMapping(value="/fillFormResponse")
    public ModelAndView showFormForResponse() throws SQLException {
        List<QuestionModel> questionList = questionFormDAO.getQuestionListForStudent("Survey");
        ModelAndView model = new ModelAndView("customFormResponse");
        model.addObject("list",questionList);
        return model;
    }

    @RequestMapping(value="/processFormResponse")
    public String showFormResponseSuccess(HttpServletRequest request) throws Exception {
        Map<String, String[]> parameterMap = request.getParameterMap();
        for (String  key : parameterMap.keySet()) System.out.println(key+"---"+parameterMap.get(key)[0]);
        int noOfQuestions = questionFormDAO.totalQuestionsInForm("Survey");
        for (int i = 1; i <= noOfQuestions; i++) {
            QuestionModel questionModel = new QuestionModel();
            AnswerModel answerModel = new AnswerModel();

            if (questionModel.getQuestionType().equals("2") || questionModel.getQuestionType().equals("4")) {
                int questionId = questionModel.getQuestionId();
//                questionModel.setOptionValue1(parameterMap.get("q_desc_" + i + "_detail_value_1")[0]);
//                questionModel.setOptionValue2(parameterMap.get("q_desc_" + i + "_detail_value_2")[0]);
//                questionModel.setOptionValue3(parameterMap.get("q_desc_" + i + "_detail_value_3")[0]);
//                questionModel.setOptionValue4(parameterMap.get("q_desc_" + i + "_detail_value_4")[0]);
                answerModel.setAnswer1(parameterMap.get("answerForQuestionId_" + questionId)[0]);
            }
//            form.getQuestions().add(questionModel);
        }
//        questionFormDAO.insertFormData(form);
        List<QuestionModel> questionList = questionFormDAO.getQuestionListForStudent("Survey");
//        ModelAndView model = new ModelAndView("customFormResponseSuccess");
//        model.addObject("list",questionList);
        return "customFormResponseSuccess";
    }
}
