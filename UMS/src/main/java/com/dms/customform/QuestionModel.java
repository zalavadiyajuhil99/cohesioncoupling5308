package com.dms.customform;

public class QuestionModel {
    private int questionId;
    private String questionType;
    private String questionDescription;
    private String optionValue1;
    private String optionValue2;
    private String optionValue3;
    private String optionValue4;

    public int getQuestionId() {
        return questionId;
    }

    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public String getQuestionType() {
        return questionType;
    }

    public void setQuestionType(String questionType) {
        this.questionType = questionType;
    }

    public String getQuestionDescription() {
        return questionDescription;
    }

    public void setQuestionDescription(String questionDescription) {
        this.questionDescription = questionDescription;
    }

    public String getOptionValue1() {
        return optionValue1;
    }

    public void setOptionValue1(String optionValue1) {
        this.optionValue1 = optionValue1;
    }

    public String getOptionValue2() {
        return optionValue2;
    }

    public void setOptionValue2(String optionValue2) {
        this.optionValue2 = optionValue2;
    }

    public String getOptionValue3() {
        return optionValue3;
    }

    public void setOptionValue3(String optionValue3) {
        this.optionValue3 = optionValue3;
    }

    public String getOptionValue4() {
        return optionValue4;
    }

    public void setOptionValue4(String optionValue4) {
        this.optionValue4 = optionValue4;
    }

    public String toString() {
        return "QuestionModel{" +
                "questionType='" + questionType + '\'' +
                ", questionDescription='" + questionDescription + '\'' +
                ", optionValue1='" + optionValue1 + '\'' +
                ", optionValue2='" + optionValue2 + '\'' +
                ", optionValue3='" + optionValue3 + '\'' +
                ", optionValue4='" + optionValue4 + '\'' +
                '}';
    }

}
