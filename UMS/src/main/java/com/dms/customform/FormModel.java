package com.dms.customform;

import java.util.List;

public class FormModel {
    private  int formId;
    private  String formName;
    private List<QuestionModel> questions;

    public int getFormId() {
        return formId;
    }

    public void setFormId(int formId) {
        this.formId = formId;
    }

    public String getFormName() {
        return formName;
    }

    public void setFormName(String formName) {
        this.formName = formName;
    }

    public List<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionModel> questions) {
        this.questions = questions;
    }

    public String toString() {
        return "FormModel{" +
                "formName='" + formName + '\'' +
                ", questions=" + questions +
                '}';
    }
}
