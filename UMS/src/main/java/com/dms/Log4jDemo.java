package com.dms;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4jDemo {
    static  Logger logger = LogManager.getLogger(Log4jDemo.class);

    public static void main(String[] args) {
        Boolean isMatch = StringUtils.equals("j","j");
        System.out.println("String matches" + isMatch);
        System.out.println("\nHello World \n");
        logger.info("This is info message");
        logger.error("This is an error message");
        logger.warn("This is warning message");
        logger.fatal("This is fatal message");
        System.out.println("Completed");
    }


}

