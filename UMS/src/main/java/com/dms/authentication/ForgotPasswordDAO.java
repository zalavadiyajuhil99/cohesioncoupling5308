package com.dms.authentication;

import com.dms.authentication.interfaces.IForgotPasswordDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.*;

public class ForgotPasswordDAO implements IForgotPasswordDAO {
    private final ISqlConnection connectionManager;

    public ForgotPasswordDAO()
    {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public boolean updatePassword(String username, String emailId, String securityAnswer, String password, String reenterPassword) throws SQLException {
        ForgotPasswordVerify forgotPasswordVerify = new ForgotPasswordVerify();
        String email = null;
        String answer = null;
        try{
            String query = "select email, securityAnswer1 from User where username='"+username+"'";
            connectionManager.executeRead(query);
            ResultSet resultSet = connectionManager.executeResult(query);
            while(resultSet.next())
            {
                email = resultSet.getString(1);
                answer = resultSet.getString(2);
                System.out.println(email+","+answer);
            }
            boolean passwordEqual = forgotPasswordVerify.passwordVerify(password,reenterPassword);
            if(email.equals(emailId) && answer.equals(securityAnswer) && passwordEqual)
            {
                String updateQuery = "update User set password = '"+password+"' where username = '"+username+"'";
                connectionManager.executeWrite(updateQuery);
                return true;
            }
        }catch (SQLException sqlException)
        {
            sqlException.printStackTrace();
        }finally {
            connectionManager.closeConnection();
        }
        return false;
    }
}
