package com.dms.authentication;

import com.dms.authentication.interfaces.IUserSignUp;
import com.dms.authentication.interfaces.IValidation;
import com.dms.user.interfaces.IUser;
import com.dms.user.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
public class SignUpController {
	private static final String SIGN_UP_PAGE = "signupForm";
	private static final String SIGN_UP_SUCCESS_PAGE = "signupSuccess";
	private static final String ERROR_PAGE = "error";

	IUserSignUp signUpDAO = new SignUpDAO();
	IValidation passwordValidation = new PasswordValidation();

	@RequestMapping(value="/signUpForm")
	public String showForm() {
		return SIGN_UP_PAGE;
	}

	@RequestMapping(value="/processForm")
	public String showSuccess(@RequestParam("username") String username,
							  @RequestParam("email") String email,
							  @RequestParam("firstName") String firstName,
							  @RequestParam("lastName") String lastName,
							  @RequestParam("dob") String dob,
							  @RequestParam("securityAnswer1") String securityAnswer,
							  @RequestParam("password") String password,
							  @RequestParam("role") String role) throws Exception {
			Date birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(dob);
			IUser user = new User(username,email, firstName, lastName, birthDate.getTime(), securityAnswer, password, role);
			if(passwordValidation.isValid(password) && user.validateDOB(dob) && user.userAgeRequirement(dob)) {
				signUpDAO.insertUser(user);
				return SIGN_UP_SUCCESS_PAGE;
			}
		return ERROR_PAGE;
	}
}






