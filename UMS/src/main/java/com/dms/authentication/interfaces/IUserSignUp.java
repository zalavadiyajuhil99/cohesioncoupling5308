package com.dms.authentication.interfaces;

import com.dms.user.interfaces.IUser;

import java.sql.SQLException;

public interface IUserSignUp {
    Boolean isUserValid(IUser user) throws SQLException;

    Boolean insertUser(IUser user) throws SQLException;

    Boolean insertInfoInSeparateTableBasedOnUserRole(IUser user) throws SQLException;
}
