package com.dms.authentication;

import com.dms.authentication.interfaces.IForgotPasswordDAO;

import java.sql.SQLException;

public class ForgotPasswordModel {
    public boolean resetPassword(String username, String emailId, String securityAnswer, String password, String reenterPassword, IForgotPasswordDAO iForgotPasswordDao) throws SQLException {
        boolean result= false;
        result =iForgotPasswordDao.updatePassword(username,emailId,securityAnswer,password,reenterPassword);
        return result;
    }
}
