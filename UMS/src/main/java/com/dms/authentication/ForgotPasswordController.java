package com.dms.authentication;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;

@Controller
public class ForgotPasswordController {
    @RequestMapping(value = "/forgotpassword")
    public String forgotPasswordPage(ModelMap model) throws SQLException {
        return "forgotpassword";
    }

    @PostMapping(value="/resetPassword")
    public String resetPassword(ModelMap model, @RequestParam("username") String userName,  @RequestParam("securityAnswer") String securityAnswer, @RequestParam("emailID") String emailID, @RequestParam("password") String password, @RequestParam("reenterPassword") String reenterPassword) throws SQLException {
        ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
        ForgotPasswordDAO forgotPasswordDao = new ForgotPasswordDAO();
        boolean passwordReset = false;
        passwordReset = forgotPasswordModel.resetPassword(userName,emailID,securityAnswer,password,reenterPassword,forgotPasswordDao);
        if(passwordReset)
        {
            return "redirect:/";
        }
        else {
            return "redirect:forgotpassword";
        }
    }
}
