package com.dms.authentication;

import com.dms.authentication.interfaces.ICaptcha;
import com.dms.authentication.interfaces.ILoginModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;

@Controller
public class LoginController {
    private static final String STUDENT_PAGE = "studentlogin";
    private static final String FACULTY_PAGE = "facultylogin";
    private static final String ADMIN_PAGE = "redirect:/admin";
    private static final String LOGGED_IN = "yes";

    LoginDAO loginDAO = new LoginDAO();
    ILoginModel iLoginModel = new LoginModel(loginDAO);
    ICaptcha iCaptcha = new CaptchaModel();

    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String showLoginPage(ModelMap model){
        String captcha = iCaptcha.generateCaptcha();
        model.addAttribute("captchainput",captcha);
        return "login";
    }

    @RequestMapping(value="/accountlogin", method = RequestMethod.POST)
    public String login(@RequestParam("UserID") String UserID, @RequestParam("password") String password, @RequestParam("captchaOutput") String captchaOutput, HttpServletRequest request) throws SQLException {
        boolean isValidCaptcha = iCaptcha.captcha(captchaOutput);
        String displayPage = iLoginModel.userValidation(UserID, password);
           if(isValidCaptcha == true &&(displayPage.equals(STUDENT_PAGE) || displayPage.equals(FACULTY_PAGE)|| displayPage.equals(ADMIN_PAGE))){
               HttpSession session = request.getSession();
               session.setAttribute("username",UserID);
               session.setAttribute("loggedIn",LOGGED_IN);
               return displayPage;
           }
           return "invalidlogin";
        }
    }
