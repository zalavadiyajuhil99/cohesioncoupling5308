package com.dms.authentication;

import com.dms.authentication.interfaces.IValidation;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PasswordValidation implements IValidation {
    private List<IValidation> validations = new ArrayList<>();

    public PasswordValidation() {
        validations.add(new MinAndMaxLengthPasswordValidation());
        validations.add(new CasePasswordValidation());
        validations.add(new DigitAndSpecialCharacterValidation());
    }

    public Boolean isValid(String password) throws Exception {
        Iterator<IValidation> iter = validations.iterator();
        while (iter.hasNext()) {
            IValidation validator = iter.next();
            if (!validator.isValid(password)) {
                return false;
            }
        }
        return true;
    }
}
