package com.dms.authentication;

import com.dms.authentication.interfaces.ICaptcha;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CaptchaModel implements ICaptcha {
    int randomNumber1 ;
    int randomNumber2 ;
    private final String operation1 ="+";
    private final String operation2 ="-";
    private final String operation3 ="*";

    String operationGenerated;
    String CaptchaInput;
    boolean CaptchaValid;
    Random rand = new Random();

    public int random1() {
        int random1 = rand.nextInt(10);
        return (random1);
    }

    public int random2() {
        int random2 = rand.nextInt(10);
        return (random2);
    }

    public String operation(){
        List<String> list=new ArrayList<String>();
        list.add(operation1);
        list.add(operation2);
        list.add(operation3);
        int randomOperation = rand.nextInt(list.size());
        return(list.get(randomOperation));
    }

    public String generateCaptcha(){
        operationGenerated = operation().replace(" ", "");
        randomNumber1 =random1();
        randomNumber2 =random2();
        CaptchaInput =randomNumber1+operationGenerated+randomNumber2;
        return CaptchaInput;
    }

    public boolean captcha(String captchaOutput){
        System.out.println(captchaOutput);

        if(operationGenerated.equals(operation3)){
            CaptchaValid= multiplyOperation(captchaOutput);
        }
        else if(operationGenerated.equals(operation1)){
            CaptchaValid = additionOperation(captchaOutput);
        }
        else if (operationGenerated.equals(operation2)){
            CaptchaValid = substractOperation(captchaOutput);

        }
        else{
            CaptchaValid = false;
        }
        return CaptchaValid;
    }

    public boolean additionOperation(String captchaOutput){
        int output = randomNumber1+randomNumber2;
        if(output == Integer.parseInt(captchaOutput)){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean substractOperation(String captchaOutput){
        int output = randomNumber1-randomNumber2;
        if(output == Integer.parseInt(captchaOutput)){
            return true;
        }
        else{
            return false;
        }
    }

    public boolean multiplyOperation(String captchaOutput){
        int output = randomNumber1*randomNumber2;
        if(output == Integer.parseInt(captchaOutput)){
            return true;
        }
        else{
            return false;
        }
    }
}




