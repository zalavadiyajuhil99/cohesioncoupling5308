package com.dms.user.interfaces;

import java.sql.SQLException;

public interface IUserDAO {
    int getUserId(String username) throws SQLException;
    String getUserEmail(int userId) throws SQLException;
}
