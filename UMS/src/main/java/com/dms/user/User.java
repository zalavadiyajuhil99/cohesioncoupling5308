package com.dms.user;

import com.dms.user.interfaces.IUser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class User implements IUser {
    private static final int MINIMUM_AGE_REQUIRED_FOR_SIGN_UP = 18;
    private static final String DATE_PATTERN = "\\d{2}-\\d{2}-\\d{4}";
    private int userId;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private long dob;
    private String securityAnswer;
    private String password;
    private String role;
    private String status;

    public User(String username, String email, String firstName, String lastName, long dob, String securityAnswer, String password, String role) {
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.securityAnswer = securityAnswer;
        this.password = password;
        this.role = role;
    }

    public User(String username, String firstName, String role, String status) {
        this.username = username;
        this.firstName = firstName;
        this.role = role;
        this.status = status;
    }

    public User() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecurityAnswer1() {
        return securityAnswer;
    }

    public void setSecurityAnswer1(String securityAnswer1) {
        this.securityAnswer = securityAnswer1;
    }

    public long getDob() {
        return dob;
    }

    public void setDob(long dob) {
        this.dob = dob;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean userAgeRequirement(String dateOfBirth) {
        Date birthDate;
        try {
            birthDate = new SimpleDateFormat("dd-MM-yyyy").parse(dateOfBirth);
            long ageInMillis = new Date().getTime() - birthDate.getTime();
            int years = (int) (TimeUnit.MILLISECONDS.toDays(ageInMillis) / 365);
            if(years < MINIMUM_AGE_REQUIRED_FOR_SIGN_UP) {
                throw new IllegalArgumentException("Your age should be greater than 18 years for signup");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Boolean validateDOB(String dateOfBirth) {
        if (dateOfBirth.matches(DATE_PATTERN)) {
            return true;
        }
        throw new IllegalArgumentException("Enter a valid Date of Birth in dd-MM-yyyy format ");
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}



