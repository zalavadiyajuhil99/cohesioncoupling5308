package com.dms.user;

import com.dms.user.interfaces.IUserDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDAO implements IUserDAO {
    private static final String USER_EMAIL_COLUMN = "email";

    private final ISqlConnection connectionManager;

    public UserDAO() {
        connectionManager = new SqlConnectionImpl();
    }

    public int getUserId(String username) throws SQLException {
        int userId = 0;
        try {
            String fetchUserId = "Select userId from User where username = '" + username + "'";
            connectionManager.executeRead(fetchUserId);
            ResultSet resultSet = connectionManager.executeResult(fetchUserId);
            while (resultSet.next()) {
                userId = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return userId;
    }

    public String getUserEmail(int userId) throws SQLException {
        String mailId = "";
        try {
            String selectQuery = "SELECT email FROM User where userId = '" + userId + "'";
            connectionManager.executeRead(selectQuery);
            ResultSet resultSet = connectionManager.executeResult(selectQuery);
            if (resultSet.next()) {
                mailId = resultSet.getString(USER_EMAIL_COLUMN);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return mailId;
    }
}
