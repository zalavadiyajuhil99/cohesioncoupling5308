package com.dms.course;
import com.dms.course.interfaces.ICourseDAO;
import com.dms.studentaccount.interfaces.IStudentAccountDAO;
import com.dms.studentaccount.interfaces.IStudentAccountModel;
import com.dms.studentaccount.StudentAccountModel;
import com.dms.studentaccount.StudentAccountDAO;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CourseDAO implements ICourseDAO {
    private static final int DEFAULT_COURSE_CREDIT = 3;
    private static final float DEFAULT_COURSE_RATING= 2.5f;
    private static final int DEFAULT_COURSE_MAX_REGISTER_CAPACITY = 60;

    private final ISqlConnection connectionManager;

    IStudentAccountDAO studentAccountDAO = new StudentAccountDAO();
    IStudentAccountModel studentAccountModel = new StudentAccountModel(studentAccountDAO);


    public CourseDAO() {
        connectionManager = new SqlConnectionImpl();
    }

    @Override
    public Boolean isCourseIdAlreadyTaken(int courseId) throws SQLException {
        try {
            String selectQuery = "SELECT courseId FROM Courses where courseId =  '" + courseId + "'";
            connectionManager.executeRead(selectQuery);
            ResultSet resultSet = connectionManager.executeResult(selectQuery);
            while (resultSet.next()) {
                int existingCourseId = resultSet.getInt("courseId");
                if (courseId == existingCourseId) {
                    return true;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return false;
    }

    @Override
    public Boolean addCourse(int courseId, String courseName, int facultyId , String timetable) throws SQLException {
        try {
            String insertQuery = "INSERT INTO Courses(courseId, courseName, facultyId, courseCredit, courseRating, courseMaxRegisterCapacity, timetable) " +
                    "VALUES('" + courseId + "','" + courseName + "','" + facultyId + "','" + DEFAULT_COURSE_CREDIT + "','" + DEFAULT_COURSE_RATING + "','" + DEFAULT_COURSE_MAX_REGISTER_CAPACITY + "','"+timetable+"')";
            connectionManager.executeWrite(insertQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnectionWithoutResultset();
        }
        return true;
    }

    @Override
    public String removeCourse(String courseName) throws SQLException {
        try {
            String deleteQuery = "Delete from Courses where courseName = '" + courseName + "'";
            connectionManager.executeWrite(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return courseName;
    }

    @Override
    public ArrayList<Course> getCourses() throws SQLException{
        Course course = null;
        ArrayList<Course> coursesList = new ArrayList<>();
        try {
            String getCourses = "SELECT * FROM Courses";
            connectionManager.executeRead(getCourses);
            ResultSet resultSet = connectionManager.executeResult(getCourses);
            while (resultSet.next()) {
                course = new Course();
                course.setCourseId(resultSet.getInt("courseId"));
                course.setCourseName(resultSet.getString("courseName"));
                coursesList.add(course);
            }
        }
        catch(SQLException e){
            e.printStackTrace();
        }
        finally {
            connectionManager.closeConnection();
        }
        return  coursesList;

    }

    @Override
    public boolean registerCourse(int courseId, String userName, int semester) throws SQLException{

        int studentId = 0;
        int courseIdRegistered = 0;
        boolean registerdSuccessfully =false;

        try {
            String studentIdQuery = "SELECT studentID FROM Student where userName = '" + userName + "'";
            connectionManager.executeRead(studentIdQuery);
            ResultSet resultSet = connectionManager.executeResult(studentIdQuery);
            while (resultSet.next()) {
                studentId = resultSet.getInt("studentID");
            }

            String checkIfUserRegisteredCourseAlready = "SELECT studentId, courseId FROM CourseRegistrationDetails where courseId = " + courseId + " and studentId =" + studentId;
            resultSet = connectionManager.executeResult(checkIfUserRegisteredCourseAlready);

            while (resultSet.next()) {
                courseIdRegistered = resultSet.getInt("courseId");
            }

            if(courseId != courseIdRegistered){
                String registrationQuery = "INSERT INTO CourseRegistrationDetails(courseId, studentId, semester) VALUES(" + courseId + "," + studentId + "," + semester + ")";
                connectionManager.executeWrite(registrationQuery);
                studentAccountModel.deductMoney(studentId);
                registerdSuccessfully = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return  registerdSuccessfully;
    }

    @Override
    public boolean withdrawCourse(int courseId, String userName) throws SQLException{
        boolean withdrawSuccess = false;
        int studentId = 0;
        try {
            String studentIdQuery = "SELECT studentID FROM Student where userName = '" + userName + "'";
            connectionManager.executeRead(studentIdQuery);
            ResultSet resultSet = connectionManager.executeResult(studentIdQuery);
            while (resultSet.next()) {
                studentId = resultSet.getInt("studentID");
            }
            String withdrawCourseQuery = "DELETE FROM CourseRegistrationDetails WHERE courseId =" + courseId + " AND studentId = " + studentId ;
            withdrawSuccess = connectionManager.executeWrite(withdrawCourseQuery);
            studentAccountModel.depositMoney(studentId);

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return withdrawSuccess;
    }

    public ArrayList<String> retrieveTimetable(String userName) throws SQLException{
        ArrayList<String> timetable = new ArrayList<>();
        try {
            String studentIdQuery = "SELECT timetable FROM Courses;";
            connectionManager.executeRead(studentIdQuery);
            ResultSet resultSet = connectionManager.executeResult(studentIdQuery);
            while (resultSet.next()) {
                timetable.add(resultSet.getString(1));
            }
        }catch (SQLException e) {
                e.printStackTrace();
            } finally {
                connectionManager.closeConnection();
            }
        return timetable;
    }
}
