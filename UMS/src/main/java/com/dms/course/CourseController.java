package com.dms.course;

import com.dms.course.interfaces.ICourseDAO;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Controller
public class CourseController {

    ICourseDAO courseRegistrationDAO = new CourseDAO();
    String timetable = "";
    @RequestMapping(value = "/addCourse", method = RequestMethod.GET)
    public ModelAndView courseList(ModelMap modelMap, HttpServletRequest request) throws SQLException {
        List<Course> courseList = courseRegistrationDAO.getCourses();
        HttpSession session = request.getSession();
        ModelAndView model = new ModelAndView("addCourse");
        model.addObject("list", courseList);
        String userName = (String) session.getAttribute("username");
        ArrayList<String> timetableList = new ArrayList<>();
        timetableList = courseRegistrationDAO.retrieveTimetable(userName);
        modelMap.addAttribute("timetablelist",timetableList);
        return model;
    }

    @RequestMapping(value = "addCourse")
    public String showCourse(ModelMap model) throws SQLException {
        return "addCourse";
    }

    @RequestMapping("ProcessAddCourse")
    public String addCourse(@RequestParam("courseId") int courseId, @RequestParam("courseName") String courseName, @RequestParam("facultyId") int  facultyId, @RequestParam("day") String[] days ,@RequestParam("startTime") String startTime,@RequestParam("endTime") String endTtime) throws SQLException {
        for(int i=0;i< days.length;i++)
        {
            if(i< days.length-1)
            {
                timetable =timetable+days[i]+", ";
            }
            else{
                timetable =timetable+days[i];
            }
        }
        timetable = timetable+ " "+startTime+" to "+endTtime;
        if (courseRegistrationDAO.isCourseIdAlreadyTaken(courseId)) {
            return "courseIdAlreadyExist";
        } else {
            courseRegistrationDAO.addCourse(courseId, courseName, facultyId, timetable);
            return "redirect:/addCourse";
        }
    }

    @RequestMapping(value = "/{courseName}/removeCourse", method = RequestMethod.GET)
    public ModelAndView removeCourse(@PathVariable String courseName) throws SQLException {
        courseRegistrationDAO.removeCourse(courseName);
        return new ModelAndView("redirect:/addCourse");
    }


    @RequestMapping(value="/courseRegistrationPage",method = RequestMethod.GET)
    public String showCourseRegistrationPage(Model model, HttpServletRequest request) throws Exception {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            System.out.println("Getting Session");
            String userID = (String) session.getAttribute("username");
            ArrayList<Course> coursesList = courseRegistrationDAO.getCourses();
            model.addAttribute("courseList",coursesList);
            return "courseRegistration";
        }
        else{
            return "homepage";
        }
    }

    @RequestMapping(value="/registerCourse/{courseId}",method = RequestMethod.GET)
    public String registerCourse(Model model, HttpServletRequest request, @PathVariable(name ="courseId") int courseId, @RequestParam("semester") int semester)
            throws Exception {

        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            boolean courseRegistered = courseRegistrationDAO.registerCourse(courseId,userName,semester);
            System.out.println("inside register course");
            if(courseRegistered) {
                return "courseRegistrationSuccessful";
            }
            else{
                return "alreadyRegisteredCourse";
            }
        }
        else {
            return "error";
        }
    }

    @RequestMapping(value="/withdrawCourse/{courseId}",method = RequestMethod.GET)
    public String withdrawCourse(Model model, HttpServletRequest request, @PathVariable(name ="courseId") int courseId)
            throws Exception {

        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            boolean withdrawSuccess = courseRegistrationDAO.withdrawCourse(courseId, userName);
            if(withdrawSuccess){
                return "withdrawSuccess";
            }
            else{
                return "wihdrawFail";
            }
        }
        else {
            return "error";
        }
    }
}
