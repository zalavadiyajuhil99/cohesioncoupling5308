package com.dms.meeting;

import com.dms.meeting.abstractfactory.MeetingFactoryImplementation;
import com.dms.meeting.interfaces.IViewMeetingsModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Controller
public class ViewMeetingsController  {
    MeetingFactoryImplementation gradesFactoryImplementation= new MeetingFactoryImplementation();
    IViewMeetingsModel iViewMeetingsModel = gradesFactoryImplementation.createViewMeetingsModel();
    private static final String NoMeetings = "No meetings scheduled to the date selected";

    @RequestMapping(value = "/viewMeetings",method = RequestMethod.GET)
    public String viewScheduledMeetings(HttpServletRequest request){
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            return "verifyScheduleMeetings";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/checkScheduleOfMeetings", method = RequestMethod.POST)
    public String viewMeetingsInfo(Model model, HttpServletRequest request,@RequestParam("checkDate") Date checkDate)throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            Map<Integer, List> meetingsList= iViewMeetingsModel.meetingsInformation(userName,checkDate);
            if(meetingsList.size()>0){
                model.addAttribute("meetingsList",meetingsList);
            }
            else{
                model.addAttribute("NoMeetings",NoMeetings);
            }
            return "viewMeetings";
        }
        return "redirect:/login";

    }
}
