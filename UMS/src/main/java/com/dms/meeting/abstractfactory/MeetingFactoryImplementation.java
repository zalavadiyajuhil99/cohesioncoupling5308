package com.dms.meeting.abstractfactory;

import com.dms.meeting.ScheduleMeetingDAO;
import com.dms.meeting.ScheduleMeetingModel;
import com.dms.meeting.ViewMeetingsDAO;
import com.dms.meeting.ViewMeetingsModel;
import com.dms.meeting.interfaces.IScheduleMeeting;
import com.dms.meeting.interfaces.IScheduleMeetingModel;
import com.dms.meeting.interfaces.IViewMeeting;
import com.dms.meeting.interfaces.IViewMeetingsModel;

public class MeetingFactoryImplementation extends MeetingFactory {
    public IScheduleMeetingModel createScheduleMeetingModel(){ return new ScheduleMeetingModel(new ScheduleMeetingDAO()); }

    public IScheduleMeeting createScheduleMeeting(){
        return new ScheduleMeetingDAO();
    }

    public IViewMeeting createViewMeeting(){
        return new ViewMeetingsDAO();
    }

    public IViewMeetingsModel createViewMeetingsModel(){
        return new ViewMeetingsModel(new ViewMeetingsDAO());
    }

}
