package com.dms.meeting.abstractfactory;

import com.dms.meeting.interfaces.IScheduleMeeting;
import com.dms.meeting.interfaces.IScheduleMeetingModel;
import com.dms.meeting.interfaces.IViewMeeting;
import com.dms.meeting.interfaces.IViewMeetingsModel;

public abstract class MeetingFactory {
    public abstract IScheduleMeetingModel createScheduleMeetingModel();
    public abstract IScheduleMeeting createScheduleMeeting();
    public abstract IViewMeeting createViewMeeting();
    public abstract IViewMeetingsModel createViewMeetingsModel();
}
