package com.dms.meeting;

import com.dms.meeting.abstractfactory.MeetingFactory;
import com.dms.meeting.abstractfactory.MeetingFactoryImplementation;
import com.dms.meeting.interfaces.IScheduleMeetingModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

@Controller
public class ScheduleMeetingController {
    MeetingFactory meetingFactory = new MeetingFactoryImplementation();
    IScheduleMeetingModel iScheduleMeetingModel = meetingFactory.createScheduleMeetingModel();
    private static final String slotBookingRejected = "selected slot is unavailable.please try another slot.";
    private static final String slotBookingAccepted = "Slot booked succesfully.";

    @RequestMapping(value = "/scheduleMeeting", method = RequestMethod.GET)
    public String getfacultyNames(Model model, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            Map<Integer, List> detailsList = iScheduleMeetingModel.facultyNamesList(userName);
            model.addAttribute("detailsList",detailsList);
            return "facultyMeetingDetails";
        }
        return "redirect:/login";

    }

    @RequestMapping(value = "/CheckSlotAvailabilty", method = RequestMethod.POST)
    public String checkAvailabiltyOFSlot(Model model, @RequestParam("facultyID") int facultyID,
                                         @RequestParam("appointmentDate") Date appointmentDate,
                                         @RequestParam("slot") String slot,HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            String userName = (String) session.getAttribute("username");
            if(iScheduleMeetingModel.validateSlots(facultyID, appointmentDate, slot, userName)){
                model.addAttribute("bookingStatus",slotBookingAccepted);
            }
            else{
                model.addAttribute("bookingStatus",slotBookingRejected);
            }
            return "scheduleMeeting";
        }
    return "redirect:/login";
    }
}
