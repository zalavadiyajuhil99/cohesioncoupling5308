package com.dms.admin;

import com.dms.admin.interfaces.IAdminDAO;
import com.dms.user.interfaces.IUser;
import com.dms.user.User;
import connection.sql.ISqlConnection;
import connection.sql.SqlConnectionImpl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AdminDAO implements IAdminDAO {
    private static final String USER_NAME_COLUMN = "username";
    private static final String FIRST_NAME_COLUMN = "firstName";
    private static final String USER_ROLE_COLUMN = "role";
    private static final String USER_STATUS_COLUMN = "status";

    private final ISqlConnection connectionManager;

    public AdminDAO() {
        connectionManager = new SqlConnectionImpl();
    }

    public List<IUser> getUserList() throws SQLException {
        List<IUser> userList = new ArrayList<>();
        try {
            String selectQuery = "select * from User";
            connectionManager.executeRead(selectQuery);
            ResultSet result = connectionManager.executeResult(selectQuery);
            while (result.next()) {
                IUser user = new User();
                user.setUsername(result.getString(USER_NAME_COLUMN));
                user.setFirstName(result.getString(FIRST_NAME_COLUMN));
                user.setRole(result.getString(USER_ROLE_COLUMN));
                user.setStatus(result.getString(USER_STATUS_COLUMN));
                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return userList;
    }

    public String approveUser(String username) throws SQLException {
        try {
            String approveQuery = "Update User set status='approved' where username = '" + username + "'";
            connectionManager.executeWrite(approveQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return username;
    }

    public String removeUser(String username) throws SQLException {
        try {
            String deleteQuery = "Delete from User where username = '" + username + "'";
            connectionManager.executeWrite(deleteQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            connectionManager.closeConnection();
        }
        return username;
    }
}
