package com.dms.admin.interfaces;

import com.dms.user.interfaces.IUser;
import java.sql.SQLException;
import java.util.List;

public interface IAdminDAO {
    List<IUser> getUserList() throws SQLException;
    String removeUser(String username) throws SQLException;
    String approveUser(String username) throws SQLException;
}
