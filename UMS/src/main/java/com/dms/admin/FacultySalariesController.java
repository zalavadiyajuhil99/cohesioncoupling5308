package com.dms.admin;

import com.dms.admin.interfaces.IFacultySalaryModel;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.Map;

@Controller
public class FacultySalariesController {
    private static final String SALARY_GENERATION_SUCESSFULL = "Salary succesfully generated.";
    private static final String SALARY_GENERATION_FAILED = "Salary generation failed. Please try agian!";

    FacultySalaryDAO facultySalaryDAO = new FacultySalaryDAO();
    IFacultySalaryModel facultySalaryModel = new FacultySalaryModel(facultySalaryDAO);

    @RequestMapping(value = "/facultySalary", method = RequestMethod.GET)
    public String facultyPayrollDetails(Model model, HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            Map<Integer, String> facultyDetails = facultySalaryModel.facultyDetails();
            model.addAttribute("facultyDetails", facultyDetails);
            return "facultySalary";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/updateFacultySalary", method = RequestMethod.POST)
    public String updateFacultySalary(Model model, HttpServletRequest request, @RequestParam("facultyId") int facultyId,
                                      @RequestParam("salary") int salary, @RequestParam("month") int month,
                                      @RequestParam("year") int year) throws SQLException {
        HttpSession session = request.getSession();
        if (session.getAttribute("loggedIn").equals("yes")) {
            if (facultySalaryModel.updateSalary(facultyId, salary, month, year)) {
                model.addAttribute("salaryGenerated",SALARY_GENERATION_SUCESSFULL );
            } else {
                model.addAttribute("salaryGenerated", SALARY_GENERATION_FAILED);
            }
            return "salaryGenerationSuccessfull";
        }
        return "redirect:/login";
    }
}
