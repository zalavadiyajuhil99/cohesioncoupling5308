package com.dms.admin;

import com.dms.admin.interfaces.IAdminDAO;
import com.dms.user.interfaces.IUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

@Controller
public class AdminController {
    private static final String ADMIN_PAGE = "admin";
    private static final String REDIRECT_ADMIN_PAGE = "redirect:/admin";
    private static final String ERROR_PAGE = "error";
    private static final String USER_LOGGED_IN = "loggedIn";
    private static final String YES = "yes";

    IAdminDAO adminDAO = new AdminDAO();

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    public ModelAndView userList(HttpServletRequest request) throws SQLException {
        HttpSession session = request.getSession();
        String loggedIn = (String) session.getAttribute(USER_LOGGED_IN);
        if (StringUtils.equals(loggedIn,YES)) {
            List<IUser> studentList = adminDAO.getUserList();
            ModelAndView model = new ModelAndView(ADMIN_PAGE);
            model.addObject("list", studentList);
            return model;
        }
        return new ModelAndView(ERROR_PAGE);
    }

    @RequestMapping(value = "/{username}/approve", method = RequestMethod.GET)
    public ModelAndView approveUser(@PathVariable String username) throws SQLException {
        adminDAO.approveUser(username);
        return new ModelAndView(REDIRECT_ADMIN_PAGE);
    }

    @RequestMapping(value = "/{username}/remove", method = RequestMethod.GET)
    public ModelAndView removeUser(@PathVariable String username) throws SQLException {
        adminDAO.removeUser(username);
        return new ModelAndView(REDIRECT_ADMIN_PAGE);
    }
}
