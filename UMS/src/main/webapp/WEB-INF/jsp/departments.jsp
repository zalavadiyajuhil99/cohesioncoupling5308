<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Welcome</title>
    <style>
        .veticalmenu {
            padding: 6px 8px 6px 16px;
            text-decoration: none;
            font-size: 20px;
            color: black;
            display: block;
            border: none;
            background-color: white;
            width: 20%;
            height: 100%;
            text-align: left;
            outline: none;
        }
    </style>
</head>
<body>
    <div><jsp:include page="navigation.jsp"/></div>
    <div class="veticalmenu">
            <c:forEach items="${departmentnames}" var="names">
               <h2>${names}</h2>
            </c:forEach>

        </div>
</body>