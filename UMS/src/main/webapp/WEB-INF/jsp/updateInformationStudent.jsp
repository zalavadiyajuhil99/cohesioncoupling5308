<%@ page import="java.sql.*" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>UpdateInformation</title>
        <style>
            .updateform{
                padding-left : 210px;
                font-family : Calibri;
            }
        </style>
    </head>
    <body>
        <div><jsp:include page="studentlogin.jsp"/></div>
        <div class="updateform">
            <form action="/updateinformation" method="POST">
                <table>
                    <%
                        Connection con = null;
                        String firstName = null,lastName = null ,email = null,password = null;
                        String username = null;
                        String dob = null;
                        try{

                            Class.forName("com.mysql.cj.jdbc.Driver");
                            con = DriverManager.getConnection("jdbc:mysql://db-5308.cs.dal.ca:3306/CSCI5308_11_DEVINT?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC","CSCI5308_11_DEVINT_USER","jBasFzM4vnKhLEWk");
                            HttpSession session1 = request.getSession();
                            username = session1.getAttribute("username").toString();
                            if(username!=null)
                            {
                                String userQuery = "select firstName,lastName,dob,email,password from User where username=?";
                                PreparedStatement preparedStatement = null;
                                preparedStatement = con.prepareStatement(userQuery);
                                preparedStatement.setString(1,username);
                                ResultSet resultSet = preparedStatement.executeQuery();

                                while(resultSet.next()){
                                    firstName=resultSet.getString(1);
                                    lastName=resultSet.getString(2);
                                    dob=resultSet.getString(3);
                                    email=resultSet.getString(4);
                                    password=resultSet.getString(5);
                                }
                             }
                        }catch (Exception e)
                        {
                            e.printStackTrace();
                         }finally {
                            con.close();
                        }
                    %>
                    <tr>
                        <td>First Name:</td>
                        <td><input type="text" name="firstName" placeholder="Enter FirstName" value="<%=firstName%>" required></td>
                    </tr>
                    <tr>
                        <td>Last Name:</td>
                        <td><input type="text" name="lastName" placeholder="Enter LastName" value="<%=lastName%>" required></td>
                    </tr>
                    <tr>
                        <td>Date of Birth:</td>
                        <td><input type="text" name="dateOfBirth" value="<%=dob%>" required></td>
                    </tr>
                    <tr>
                        <td>Email ID:</td>
                        <td><input type="text" name="email" placeholder="Enter EmailID" value="<%=email%>" required></td>
                    </tr>

                    <tr>
                        <td>Password:</td>
                        <td><input type="password" name="password" placeholder="Enter Password" value="<%=password%>" required></td>
                    </tr>
                    <tr>
                        <td>Reenter Password:</td>
                        <td><input type="password" name="reenterPassword" placeholder="ReEnter Password" required></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Submit"></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>