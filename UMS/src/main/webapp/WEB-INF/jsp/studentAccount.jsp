<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <title>Student Login</title>
</head>
<style>
    .container {
        display: flex;
        align-items: center;
        justify-content: center
    }
    ul {
        float: left;
        list-style-type: none;
        margin: 0;
        padding: 0;
        width: 200px;
        height : 100%;
        background-color: #1c313b;
    }

    li a {
        display: block;
        color: white;
        padding: 8px 16px;
        text-decoration: none;

    }

    /* Change the link color on hover */
    li a:hover {
        background-color: #555;
        color: white;
    }
    .text{
        padding-left : 210px;
    }
</style>
<body>
<div><jsp:include page="studentlogin.jsp"/></div>
<div class="text">
    <h2>Welcome, </h2>
    <h2> student account page</h2>
    <table>
        <tr>
            <td>
               Total Amount
            </td>
            <td>
                Billed Amount
            </td>
            <td>
                Unbilled Amount
            </td>
        </tr>
        <tr>
            <td>${studentAccount.totalAmount}</td>
            <td>${studentAccount.billedAmount}</td>
            <td>${studentAccount.unbilledAmount}</td>

        </tr>


    </table>
</div>
</body>
</html>
