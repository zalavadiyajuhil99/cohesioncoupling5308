package com.dms.library;

import com.dms.library.interfaces.IBook;
import com.dms.library.interfaces.IBookDAO;

import java.util.ArrayList;
import java.util.List;

public class BookMock implements IBookDAO {

    public List<IBook> getBookList() {
        List<IBook> bookList = new ArrayList<>();
        IBook book = new Book("Computer Networking","Prakash");
        bookList.add(book);
        return bookList;
    }

    public Boolean addBook(String bookName, String bookAuthor, int bookQuantity) {
        if (bookName.equals("ASDC") && bookAuthor.equals("Rob") && bookQuantity == 3) {
            return true;
        }
        return false;
    }

    public String removeBook(String bookName) {
        IBook book = new Book();
        if (book.getBookName() == "Data_Warehouse") {
            return book.getBookName();
        }
        return bookName;
    }

    public List<IBook> searchBook(String text) {
        List<IBook> bookList = new ArrayList<>();
        IBook book = new Book("Computer Networking");
        bookList.add(book);
        return bookList;
    }

    public Boolean validateDatesForBookRequest(String issueDate, String returnDate) {
        if(issueDate == "15-02-2021"&& returnDate == "25-02-2021") {
            return true;
        }
        return false;
    }

    public boolean requestBook(String username, int bookID, long dateToBeChecked, long returnDate) {
            if (username == "Juhil" && bookID == 5 && dateToBeChecked == 10-12-2020 && returnDate == 25-12-2020) {
                    return true;
                }
        return false;
    }

    public boolean notifyUserForBook(int bookId) {
            if (bookId == 5) {
                return true;
            }
        return false;
    }

}
