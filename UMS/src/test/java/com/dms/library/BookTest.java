package com.dms.library;

import com.dms.library.interfaces.IBook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class BookTest {
    IBook book;
    BookMock bookMock = new BookMock();
    IBookOperationMock bookOperationMock = new IBookOperationMock();

    @BeforeEach
    void init() {
        book = new Book();
    }

    @Test
    void getBookName() {
        String bookName = "SOLID Principals";
        book.setBookName(bookName);
        Assertions.assertNotNull(book.getBookName());
        Assertions.assertEquals(book.getBookName(),bookName);
    }

    @Test
    void setBookName() {
        String bookName = "Design Patterns";
        book.setBookName(bookName);
        Assertions.assertNotNull(book.getBookName());
        Assertions.assertEquals(book.getBookName(),bookName);
    }

    @Test
    void getBookAuthor() {
        String bookAuthor = "Rob";
        book.setBookAuthor(bookAuthor);
        Assertions.assertNotNull(book.getBookAuthor());
        Assertions.assertEquals(book.getBookAuthor(),bookAuthor);
    }

    @Test
    void setBookAuthor() {
        String bookAuthor = "Andrew";
        book.setBookAuthor(bookAuthor);
        Assertions.assertNotNull(book.getBookAuthor());
        Assertions.assertEquals(book.getBookAuthor(),bookAuthor);
    }

    @Test
    void getBookId() {
        int bookId = 4;
        book.setBookId(bookId);
        Assertions.assertNotNull(book.getBookId());
        Assertions.assertEquals(book.getBookId(),bookId);
    }

    @Test
    void setBookId() {
        int bookId = 18;
        book.setBookId(bookId);
        Assertions.assertNotNull(book.getBookId());
        Assertions.assertEquals(book.getBookId(),bookId);
    }

    @Test
    void getBookQuantity() {
        int bookQuantity = 6;
        book.setBookQuantity(bookQuantity);
        Assertions.assertNotNull(book.getBookQuantity());
        Assertions.assertEquals(book.getBookQuantity(),bookQuantity);
    }

    @Test
    void setBookQuantity() {
        int bookQuantity = 2;
        book.setBookQuantity(bookQuantity);
        Assertions.assertNotNull(book.getBookQuantity());
        Assertions.assertEquals(book.getBookQuantity(),bookQuantity);
    }

    @Test
    void getIssueDate() {
        String issueDate = "15-01-2021";
        book.setIssueDate(issueDate);
        Assertions.assertNotNull(book.getIssueDate());
        Assertions.assertEquals(book.getIssueDate(),issueDate);
    }

    @Test
    void setIssueDate() {
        String issueDate = "27-02-2021";
        book.setIssueDate(issueDate);
        Assertions.assertNotNull(book.getIssueDate());
        Assertions.assertEquals(book.getIssueDate(),issueDate);
    }

    @Test
    void getReturnDate() {
        String returnDate = "07-03-2021";
        book.setReturnDate(returnDate);
        Assertions.assertNotNull(book.getReturnDate());
        Assertions.assertEquals(book.getReturnDate(),returnDate);
    }

    @Test
    void setReturnDate() {
        String returnDate = "30-02-2021";
        book.setReturnDate(returnDate);
        Assertions.assertNotNull(book.getReturnDate());
        Assertions.assertEquals(book.getReturnDate(),returnDate);
    }

    @Test
    void requestBook() {
        Assertions.assertTrue(bookMock.requestBook("Juhil",5,10-12-2020,25-12-2020));
    }

    @Test
    void notifyUserForBook() {
        bookOperationMock.getAvailableBookCount(5);
        Assertions.assertTrue(bookMock.notifyUserForBook(5));
    }

    @Test
    void validateDatesForBookRequest() {
        Assertions.assertTrue(book.validateDatesForBookRequest("15-02-2021","25-02-2021"));
        Assertions.assertThrows(IllegalArgumentException.class,() -> book.validateDatesForBookRequest("15-02-2021","05-02-2021"));
    }
}