package com.dms.library;

import com.dms.library.interfaces.IBook;
import com.dms.library.interfaces.IBookOperation;
import com.dms.user.interfaces.IUser;
import com.dms.user.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IBookOperationMock implements IBookOperation {
    IUser user = new User();
    IBook book = new Book();

    @Override
    public List<IBook> getIssuedBookListForUser(String username) {
        IBook book = new Book(14, "Fundamentals of CS", "15-02-2020", "27-02-2020");
        List<IBook> userBookList = new ArrayList<>();
        userBookList.add(book);
        return userBookList;
    }

    @Override
    public boolean checkBookAvailability(int bookId, long dateToBeChecked, long returnDate) {
        boolean availability = true;
        dateToBeChecked = 21 - 03 - 2021;
        returnDate = 28 - 03 - 2021;
        long issueDate = 17 - 03 - 2021;
        long bookReturn = 30 - 03 - 2021;

        if ((dateToBeChecked > issueDate && dateToBeChecked < bookReturn)
                || (returnDate > issueDate && returnDate < bookReturn)) {
            availability = false;
        }
        return availability;
    }

    @Override
    public Boolean issueBook(String username, int bookId, long issueDate, long returnDate) {
        if(user.getUsername() == "Parth" && book.getBookId() == 20 && issueDate == 20-11-2020 && returnDate == 07-12-2020 ) {
            return true;
        }
      return false;
    }

    @Override
    public int getAvailableBookCount(int bookId) {
        if(bookId == 5) {
            return 2;
        }
        return 0;
    }

    @Override
    public boolean returnBook(int bookId) {
        if (bookId == 47) {
            return true;
        }
        return false;
    }

    @Override
    public boolean addBookCount(int bookId) {
        if (bookId == 37) {
            return true;
        }
        return false;
    }

    @Override
    public boolean reduceBookCount(int bookId) {
        if (bookId == 12) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isBookAlreadyIssued(String username, int bookId) {
        if(username == "Juhil" && bookId == 53) {
            return true;
        }
        return false;
    }

    @Override
    public boolean bookNotificationRequest(String username, int bookId) throws SQLException {
        return false;
    }

    @Override
    public boolean notifyUsersForBookAvailability(int bookId) throws SQLException {
        return false;
    }

    @Override
    public boolean deleteNotifyRequestFromBookNotification(int userId) throws SQLException {
        return false;
    }
}
