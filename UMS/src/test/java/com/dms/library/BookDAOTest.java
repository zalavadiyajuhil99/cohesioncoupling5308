package com.dms.library;

import com.dms.library.interfaces.IBook;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class BookDAOTest {
    BookMock bookMock = new BookMock();

    @Test
    void getBookListTest() {
        List<IBook> bookList = new ArrayList<>();
        IBook book = new Book("Computer Networking","Prakash");
        bookList.add(book);
        Assertions.assertEquals(bookList.size(),bookMock.getBookList().size());
    }

    @Test
    void addBookTest() {
        Assertions.assertTrue(bookMock.addBook("ASDC","Rob",3));
    }

    @Test
    void removeBookTest() {
        String result = bookMock.removeBook("Data_Warehouse");
        Assertions.assertEquals("Data_Warehouse", result);
    }

    @Test
    void searchBookTest() {
        List<IBook> searchBookList = new ArrayList<>();
        IBook book = new Book("Computer Networking");
        searchBookList.add(book);
        Assertions.assertEquals(searchBookList.size(),bookMock.searchBook("Pratik").size());
    }
}