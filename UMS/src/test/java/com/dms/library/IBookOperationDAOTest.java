package com.dms.library;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class IBookOperationDAOTest {
    IBookOperationMock bookOperationMock = new IBookOperationMock();

    @Test
    void getIssuedBookListForUserTest() {
        Book book = new Book(14, "Fundamentals of CS", "15-02-2020", "27-02-2020");
        List<Book> userBookList = new ArrayList<>();
        userBookList.add(book);
        Assertions.assertEquals(userBookList.size(),bookOperationMock.getIssuedBookListForUser("Pratik").size());
    }

    @Test
    void checkBookAvailabilityTest() {
        Assertions.assertFalse(bookOperationMock.checkBookAvailability(21,21 - 03 - 2021,28 - 03 - 2021));
    }

    @Test
    void issueBookTest() {
        Assertions.assertFalse(bookOperationMock.issueBook("Parth", 20, 20-11-2020, 15-12-2020));
    }

    @Test
    void getAvailableBookCountTest() {
        int count = bookOperationMock.getAvailableBookCount(5);
        Assertions.assertEquals(2,count);
    }

    @Test
    void returnBookTest() {
        Assertions.assertTrue(bookOperationMock.returnBook(47));
    }

    @Test
    void addBookCountTest() {
        Assertions.assertTrue(bookOperationMock.addBookCount(37));
    }

    @Test
    void reduceBookCountTest() {
        Assertions.assertTrue(bookOperationMock.reduceBookCount(12));
    }

    @Test
    void isBookAlreadyIssuedTest() {
        Assertions.assertTrue(bookOperationMock.isBookAlreadyIssued("Juhil",53));
    }
}