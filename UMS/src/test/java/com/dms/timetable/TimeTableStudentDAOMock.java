package com.dms.timetable;

import com.dms.timetable.interfaces.ITimetableStudentDAO;

import java.sql.SQLException;
import java.util.HashMap;

public class TimeTableStudentDAOMock implements ITimetableStudentDAO {
    @Override
    public HashMap<Integer, String> getTimetable(String username) throws SQLException {
        HashMap<Integer,String> timetable = new HashMap<>();
        if(username.equals("Parth"))
        {
            timetable.put(5308,"Monday 10:00am to 11:30am");
        }
        return timetable;
    }
}
