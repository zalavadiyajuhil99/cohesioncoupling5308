package com.dms.timetable;

import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TimeTableStudentDAOTest {
    @Test
    public void getTimetable() throws SQLException {
        TimeTableStudentDAOMock timeTableStudentDaoMock = new TimeTableStudentDAOMock();
        HashMap<Integer,String> timetable = new HashMap<>();
        TimeTableStudentModel timeTableStudentModel = new TimeTableStudentModel();
        timetable = timeTableStudentModel.getTimetable("Parth",timeTableStudentDaoMock);
        assertEquals("Monday 10:00am to 11:30am",timetable.get(5308));
    }
}
