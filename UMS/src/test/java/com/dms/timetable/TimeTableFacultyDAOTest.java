package com.dms.timetable;

import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.HashMap;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TimeTableFacultyDAOTest {
    @Test
    public void getTimetable() throws SQLException {
       TimeTableFacultyDAOMock timeTableFacultyDaoMock = new TimeTableFacultyDAOMock();
        HashMap<Integer,String> timetable = new HashMap<>();
        TimeTableFacultyModel timeTableFacultyModel = new TimeTableFacultyModel();
        timetable = timeTableFacultyModel.getTimetable("faculty",timeTableFacultyDaoMock);
        assertEquals("Monday 10:00am to 11:30am",timetable.get(5308));
    }
}
