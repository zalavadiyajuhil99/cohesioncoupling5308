package com.dms.timetable;

import com.dms.timetable.interfaces.ITimeTableFacultyDAO;

import java.sql.SQLException;
import java.util.HashMap;

public class TimeTableFacultyDAOMock implements ITimeTableFacultyDAO {
    @Override
    public HashMap<Integer, String> getTimetable(String username) throws SQLException {
        HashMap<Integer,String> timetable = new HashMap<>();
        if(username.equals("faculty"))
        {
            timetable.put(5308,"Monday 10:00am to 11:30am");
        }
        return timetable;
    }
}
