package com.dms.admin;

import com.dms.admin.interfaces.IFacultySalaryModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;

class FacultySalaryModelTest {
    IFacultySalaryModel iFacultySalaryModel;
    FacultySalaryDAOMock facultySalaryDAOMock;
    @BeforeEach
    void init() {
        facultySalaryDAOMock = new FacultySalaryDAOMock();
        iFacultySalaryModel = new FacultySalaryModel(facultySalaryDAOMock);

    }
        @Test
        void convertMonthTest(){
            Assertions.assertEquals("April",iFacultySalaryModel.convertMonth(4));
        }
        @Test
        void facultydetailsTest() throws SQLException {
            Map<Integer, String> facultyDetails= iFacultySalaryModel.facultyDetails();
            assertAll(
                    () -> Assertions.assertEquals(10000, facultyDetails.keySet().toArray()[0]),
                    () -> Assertions.assertEquals("Professor", facultyDetails.values().toArray()[0])
            );
        }
        @Test
         void updateSalaryTest(){
             assertAll(
                () -> Assertions.assertEquals(true,iFacultySalaryModel.updateSalary(11111,4000,1,2021)),
                () -> Assertions.assertEquals(false,iFacultySalaryModel.updateSalary(11111,2000,1,2021)),
                () -> Assertions.assertEquals(true,iFacultySalaryModel.updateSalary(10000,4000,1,2021)),
                () -> Assertions.assertEquals(false,iFacultySalaryModel.updateSalary(10000,2000,1,2021))
             );
    }
}