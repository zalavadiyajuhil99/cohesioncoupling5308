package com.dms.admin;

import com.dms.admin.interfaces.IAdminDAO;
import com.dms.user.interfaces.IUser;
import com.dms.user.User;

import java.util.ArrayList;
import java.util.List;

public class AdminMock implements IAdminDAO {
    IUser user = new User();

    public List<IUser> getUserList() {
        IUser user = new User("Pratik","Pratik","student","approve");
        List<IUser> userList = new ArrayList<>();
        userList.add(user);
        return userList;
    }

    public String removeUser(String username) {
        if(user.getUsername() == "Sahil") {
            return user.getUsername();
        }
        return username;
    }

    public String approveUser(String username) {
        if(user.getUsername() == "Parth") {
            return user.getUsername();
        }
        return username;
    }
}
