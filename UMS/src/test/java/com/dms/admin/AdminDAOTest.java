package com.dms.admin;

import com.dms.user.interfaces.IUser;
import com.dms.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class AdminDAOTest {
    AdminMock adminMock = new AdminMock();

    @Test
    void getUserListTest() {
        IUser user = new User("Pratik","Pratik","student","approve");
        List<IUser> userList = new ArrayList<>();
        userList.add(user);
        Assertions.assertEquals(userList.size(),adminMock.getUserList().size());
    }

    @Test
    void approveUserTest() {
        String result = adminMock.approveUser("Parth");
        Assertions.assertEquals("Parth", result);
    }

    @Test
    void removeUserTest() {
        String result = adminMock.removeUser("Sahil");
        Assertions.assertEquals("Sahil", result);
    }
}