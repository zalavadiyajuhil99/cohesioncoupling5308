package com.dms.admin;

import com.dms.admin.interfaces.IFacultySalary;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class FacultySalaryDAOMock implements IFacultySalary {

    @Override
    public Map<Integer, String> getFacultySalaryDetails() throws SQLException{
        Map<Integer, String> facultyDetails = new HashMap<Integer,String>();
        facultyDetails.put(10000,"Professor");
        return facultyDetails;
    }

    @Override
    public float getCourseRatingAvg( int FacultyID) throws SQLException{
        float courseRatingAvg = 0;
        if(FacultyID == 11111){
            courseRatingAvg = 4;
            return courseRatingAvg;

        }
        else {
            return courseRatingAvg;
        }

    }
    @Override
    public  boolean insertFacultySalary(int facultyID, int salary, String month, int year) throws SQLException{
        boolean salaryInserted = false;
        if(facultyID == 10000 && salary == 4000 && month == "January" && year == 2021){
            salaryInserted = true;
        }
        return salaryInserted;
    }

    @Override
    public boolean checkFacultySalary(int facultyID, String month, int year) throws SQLException{
        boolean salaryExists= false;
        if(facultyID == 11111 && month == "January" && year == 2021){
            salaryExists= true;
        }
        else if (facultyID == 10000 && month == "January" && year == 2021){
            salaryExists =  false;
        }
        return salaryExists;
    }

    @Override
    public boolean updateFacultySalary(int facultyID, int salary, String month, int year) throws SQLException{
        boolean salaryUpdated = false;
        if(facultyID == 11111 && salary == 4000 && month == "January" && year == 2021){
            salaryUpdated = true;
        }
        return salaryUpdated;
    }
}
