package com.dms.courserating;

import com.dms.courserating.interfaces.ICourseRatingModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;


class CourseRatingModelTest {
    ICourseRatingModel courseRating;
    CourseRatingDaoMock courseRatingDaoMock;


    @BeforeEach
    void init() {
        courseRatingDaoMock = new CourseRatingDaoMock();
        courseRating = new CourseRatingModel(courseRatingDaoMock);
    }

    @Test
    void postCourseRating() throws SQLException {
        boolean posCourseRatingTestFlag = courseRating.postCourseRating( 5000 ,"Moksh" ,4);
        Assertions.assertTrue(posCourseRatingTestFlag);
    }
}