package com.dms.authentication;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForgotPasswordVerifyTest {
    @Test
    public void passwordMatch() {
        ForgotPasswordVerify forgotPasswordVerify = new ForgotPasswordVerify();
        assertEquals(true   ,forgotPasswordVerify.passwordVerify("password","password"));
    }
}
