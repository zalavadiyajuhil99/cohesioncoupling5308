package com.dms.authentication;

import com.dms.authentication.interfaces.IUserSignUp;
import com.dms.user.interfaces.IUser;

public class SignUpMock implements IUserSignUp {

    public Boolean isUserValid(IUser user) {
        if(user.getUsername().equals("Pratik")) {
            return true;
        }
        return false;
    }

    public Boolean insertUser(IUser user) {
        if(user.getUsername() == "Pratik" &&
                user.getEmail() == "pratil@gmail.com" &&
                user.getFirstName() == "Pratik" &&
                user.getLastName() == "Zala" &&
                user.getDob() == 15-01-1999 &&
                user.getSecurityAnswer1() == "farari" &&
                user.getPassword() == "Pratik@123" &&
                user.getRole() == "student") {
            return true;
        }
        return false;
    }

    public Boolean insertInfoInSeparateTableBasedOnUserRole(IUser user) {
        if(user.getRole() == "student" || user.getRole() == "faculty") {
            return true;
        }
        return false;
    }
}
