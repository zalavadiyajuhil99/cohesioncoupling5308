package com.dms.authentication;

import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ForgotPasswordDAOTest {
    @Test
    public void getAttendance() throws SQLException {
        ForgotPasswordModel forgotPasswordModel = new ForgotPasswordModel();
        ForgotPasswordDAOMock forgotPasswordDaoMock = new ForgotPasswordDAOMock();
        assertEquals(true,forgotPasswordModel.resetPassword("ashwin","jemish@gmail.com","honda","password","password",forgotPasswordDaoMock));
    }
}
