package com.dms.authentication;

import com.dms.authentication.CaptchaModel;
import com.dms.authentication.interfaces.ICaptcha;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CaptchaModelTest {
    ICaptcha iCaptcha;
    @BeforeEach
    void init(){
        iCaptcha = new CaptchaModel();
    }
    @Test
    void random1Test(){
        int random1;
        boolean validNumber;
        random1 =iCaptcha.random1();
        if(random1>=0 && random1<10){
            validNumber = true;
        }
        else{
            validNumber =false;
        }
        Assertions.assertEquals(true,validNumber);
    }

    @Test
    void random2Test(){
        int random2;
        boolean validNumber;
        random2 =iCaptcha.random2();
        if(random2>=0 && random2<10){
            validNumber = true;
        }
        else{
            validNumber =false;
        }
        Assertions.assertEquals(true,validNumber);
    }

    @Test
    void operationTest(){
        String operation;
        boolean validOperation;
        operation =iCaptcha.operation();
        if(operation == "+" || operation == "-" || operation == "*"){
            validOperation = true;
        }
        else{
            validOperation =false;
        }
        Assertions.assertEquals(true,validOperation);
    }



}