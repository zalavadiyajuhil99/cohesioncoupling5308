package com.dms.authentication;

import com.dms.user.interfaces.IUser;
import com.dms.user.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class SignUpDAOTest {
    IUser user = new User("Pratik","pratil@gmail.com","Pratik",
            "Zala", 15-01-1999, "farari","Pratik@123", "student");
    @Test
    void isUserValidTest() {
        SignUpMock signUpMock = new SignUpMock();
        Assertions.assertTrue(signUpMock.isUserValid((User) user));
    }

    @Test
    void insertUserTest() {
        SignUpMock signUpMock = new SignUpMock();
        Assertions.assertTrue(signUpMock.insertUser((User) user));
    }

    @Test
    void testInsertInfoInSeparateTableBasedOnUserRoleTest() {
        SignUpMock signUpMock = new SignUpMock();
        Assertions.assertTrue(signUpMock.insertInfoInSeparateTableBasedOnUserRole((User) user));
    }
}