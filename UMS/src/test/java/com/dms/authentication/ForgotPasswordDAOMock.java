package com.dms.authentication;

import com.dms.authentication.interfaces.IForgotPasswordDAO;
import java.sql.SQLException;

public class ForgotPasswordDAOMock implements IForgotPasswordDAO {
    @Override
    public boolean updatePassword(String username, String emailId, String securityAnswer, String password, String reenterPassword) throws SQLException {
        if(username.equals("ashwin") && emailId.equals("jemish@gmail.com") && securityAnswer.equals("honda"))
        {
            return true;
        }
        else{
            return false;
        }
    }
}
