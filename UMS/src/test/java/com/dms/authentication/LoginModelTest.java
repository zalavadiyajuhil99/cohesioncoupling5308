package com.dms.authentication;

import com.dms.authentication.interfaces.ILoginModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertAll;

class LoginModelTest {
    ILoginModel iLoginModel;
    @BeforeEach
    void init() {
        iLoginModel = new LoginModel(new LoginDAOMock());
    }

    @Test
    void userValidationTest(){
        assertAll(
                () -> Assertions.assertEquals("studentlogin", iLoginModel.userValidation("bhargav","bhargav@123")),
                () -> Assertions.assertEquals("invalidlogin", iLoginModel.userValidation("bhargav1","bhargav@123"))
        );
    }
}