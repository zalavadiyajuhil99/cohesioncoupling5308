package com.dms.grade;

import com.dms.grade.interfaces.IPostGradesModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;

class PostGradesModelTest {
    IPostGradesModel iPostGradesModel;
    PostGradesDAOMock postGradesDAOMock;

    @BeforeEach
    void init() {
        postGradesDAOMock = new PostGradesDAOMock();
        iPostGradesModel = new PostGradesModel(postGradesDAOMock);
    }

    @Test
    void courseNamesListTest() throws SQLException {
        List<String> courseNames;
        courseNames = iPostGradesModel.courseNamesList("bhargav");
        Assertions.assertEquals("CloudComputing", courseNames.get(0));
        courseNames = iPostGradesModel.courseNamesList("bhargav1");
        Assertions.assertEquals("NotAvailable",courseNames.get(0));
    }

    @Test
    void getStudentDetailsTest() throws SQLException {
        Map<Integer, List<Integer>> studentDetails = iPostGradesModel.getStudentDetails("bhargav","cloudComputing");
        Assertions.assertEquals(202101, studentDetails.keySet().toArray()[0]);
        int courseId = studentDetails.get(202101).get(0);
        Assertions.assertEquals(5308,courseId);
        int marks = studentDetails.get(202101).get(1);
        Assertions.assertEquals(70,marks);
    }

    @Test
    void postGradesTest() throws SQLException{
        assertAll(
                () -> Assertions.assertEquals(true,iPostGradesModel.postGrades(70,202101,5308)),
                () -> Assertions.assertEquals(false,iPostGradesModel.postGrades(22,22222,5308))
        );
    }

}






