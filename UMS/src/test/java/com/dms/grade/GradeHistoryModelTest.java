package com.dms.grade;

import com.dms.grade.interfaces.IGradeHistoryModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

class GradeHistoryModelTest {
    IGradeHistoryModel iGradeHistoryModel;
    GradeHistoryDAOMock gradeHistoryDAOMock;

    @BeforeEach
    void init() {
        gradeHistoryDAOMock = new GradeHistoryDAOMock();
        iGradeHistoryModel = new GradeHistoryModel(gradeHistoryDAOMock);
    }

    @Test
    void courseNamesListTest() throws SQLException{
        List courseNames = iGradeHistoryModel.courseNamesList("bhargav");
        Assertions.assertEquals("cloudComputing",courseNames.get(0));
    }
    @Test
    void calculateGradesTest() throws SQLException {
        Map<Integer,String> courseGrades = iGradeHistoryModel.calculateGrades("bhargav","cloudComputing");
        Assertions.assertEquals(5308,courseGrades.keySet().toArray()[0]);
        Assertions.assertEquals("A+",courseGrades.values().toArray()[0]);
    }

    @Test
    void scoreToLetterGradeTest() {
        Assertions.assertEquals("A+", iGradeHistoryModel.scoreToLetterGrade(92));
    }

    @Test
    void calculateCgpaTest() throws SQLException {
        Assertions.assertEquals(10.0,iGradeHistoryModel.calculateCgpa("bhargav",1));
        Assertions.assertEquals(0.0,iGradeHistoryModel.calculateCgpa("bhargav1",1));
    }

}