package com.dms.course;

import com.dms.course.interfaces.ICourseDAO;

import java.sql.SQLException;
import java.util.ArrayList;

public class CourseDAOMock implements ICourseDAO {


    @Override
    public Boolean isCourseIdAlreadyTaken(int courseId) throws SQLException {
        return null;
    }

    @Override
    public Boolean addCourse(int courseId, String courseName, int facultyId, String timetable) throws SQLException {
        return null;
    }

    @Override
    public String removeCourse(String courseName) throws SQLException {
        return null;
    }

    @Override
    public  boolean registerCourse(int courseId, String userName, int semester) throws SQLException{
        if(courseId == 5000 && userName == "Moksh" && semester==1){
            return true;
        }
        else{
            return false;
        }

    };

    @Override
    public ArrayList<Course> getCourses() throws SQLException{
        ArrayList<Course> courseList = new ArrayList<>();
        Course course = new Course();
        course.setCourseId(5906);
        course.setCourseName("abc");
        courseList.add(course);
        return courseList;

    }

    @Override
    public boolean withdrawCourse(int courseId, String userName) throws SQLException{
        if(courseId == 5000 && userName == "Moksh"){
            return true;
        }
        else{
            return false;
        }


    }

    @Override
    public ArrayList<String> retrieveTimetable(String username) throws SQLException {
        ArrayList<String> timetable = new ArrayList<>();
        if (username.equals("admin")) {
            timetable.add("Monday 13:00 to 14:30");
            return timetable;
        }
        return timetable;
    }
}
