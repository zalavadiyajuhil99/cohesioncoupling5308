package com.dms.course;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

class CourseDAOTest {
    CourseDAOMock courseRegistrationMock = new CourseDAOMock();

    @Test
    void getCourses() throws SQLException {
        ArrayList<Course> courseList =  courseRegistrationMock.getCourses() ;
        Assertions.assertEquals(1,courseList.size());

    }

    @Test
    void registerCourse() throws SQLException {
        boolean flag = courseRegistrationMock.registerCourse(5000,"Moksh",1);
        Assertions.assertEquals(true,flag);
    }

    @Test
    void withdrawCourse() throws  SQLException {
        boolean flag = courseRegistrationMock.withdrawCourse(5000,"Moksh");
        Assertions.assertEquals(true,flag);
    }

    @Test
    void retrieveTimetable() throws SQLException{
        ArrayList<String> timetable = courseRegistrationMock.retrieveTimetable("admin");
        Assertions.assertEquals("Monday 13:00 to 14:30",timetable.get(0));
    }
}
