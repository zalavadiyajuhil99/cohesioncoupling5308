package com.dms.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class UserDAOTest {
    UserDaoMock userDaoMock = new UserDaoMock();

    @Test
    void getUserIdTest() {
       int userId =  userDaoMock.getUserId("Juhil");
        Assertions.assertEquals(25,userId);
    }
}