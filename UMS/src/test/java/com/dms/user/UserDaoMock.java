package com.dms.user;

import com.dms.user.interfaces.IUserDAO;

import java.sql.SQLException;

public class UserDaoMock implements IUserDAO {

    public int getUserId(String username) {
        if(username == "Juhil") {
            return 25;
        }
      return 0;
    }

    @Override
    public String getUserEmail(int userId) throws SQLException {
        return null;
    }
}
