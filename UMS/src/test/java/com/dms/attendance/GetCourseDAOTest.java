package com.dms.attendance;

import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class GetCourseDAOTest {
    @Test
    public void getCourseIDTest() throws SQLException {
        GetCourseDAOMock getCourseDaoMock = new GetCourseDAOMock();
        ArrayList<Integer> courseId = new ArrayList<>();
        GetCourseModel getCourseModel = new GetCourseModel();
        courseId=getCourseModel.getCourseID("Parth",getCourseDaoMock);
        assertEquals(5100,courseId.get(0).intValue());
    }

}
