package com.dms.attendance;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.sql.SQLException;

public class GetAttendanceDAOTest {
    @Test
    public void getAttendance() throws SQLException {
        GetAttendanceDAOMock getAttendanceDaoMock = new GetAttendanceDAOMock();
        GetAttendanceModel getAttendanceModel = new GetAttendanceModel();
        assertEquals(50,getAttendanceModel.retrievingAttendance("Parth",5100,getAttendanceDaoMock));
    }
}
