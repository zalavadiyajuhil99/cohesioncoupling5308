package com.dms.attendance;

import com.dms.attendance.interfaces.IGetCourseDAO;

import java.sql.SQLException;
import java.util.ArrayList;

public class GetCourseDAOMock implements IGetCourseDAO {
    @Override
    public ArrayList<Integer> getCourseID(String username) throws SQLException {
        ArrayList<Integer> course_id = new ArrayList<>();
        if(username.equals("Parth")) {

            course_id.add(5100);
            return course_id;
        }
        else {
            course_id.add(-1);
            return course_id;
        }
    }
}
