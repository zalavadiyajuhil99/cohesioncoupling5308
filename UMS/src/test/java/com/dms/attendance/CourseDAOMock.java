package com.dms.attendance;

import com.dms.attendance.interfaces.IPostCourseDAO;

import java.sql.SQLException;
import java.util.ArrayList;

public class CourseDAOMock implements IPostCourseDAO {
    @Override
    public ArrayList<Integer> getCourseIDForFaculty(String username) throws SQLException {
        ArrayList<Integer> course_id = new ArrayList<>();
        if(username.equals("robert")) {

            course_id.add(5308);
            return course_id;
        }
        else {
            course_id.add(-1);
            return course_id;
        }
    }
}
