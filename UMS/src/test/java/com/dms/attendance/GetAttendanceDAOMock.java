package com.dms.attendance;

import com.dms.attendance.interfaces.IGetAttendanceDAO;

import java.sql.SQLException;

public class GetAttendanceDAOMock implements IGetAttendanceDAO {
    @Override
    public int getAttendance(String username, int courseId) throws SQLException {
        if(username.equals("Parth") && courseId==5100)
        {
            return 50;
        }
        else{
            return -1;
        }

    }
}
