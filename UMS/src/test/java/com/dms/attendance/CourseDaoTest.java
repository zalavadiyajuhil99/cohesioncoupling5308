package com.dms.attendance;

import org.junit.jupiter.api.Test;

import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class CourseDaoTest {
    @Test
    public void getCourseIDForFaculty() throws SQLException{
        CourseDAOMock courseDaoMock = new CourseDAOMock();
        ArrayList<Integer> courseId = new ArrayList<>();
        PostCourseModel courseModel = new PostCourseModel();
        courseId = courseModel.getCourseIDForFaculty("robert",courseDaoMock);
        assertEquals(5308,courseId.get(0).intValue());
    }

}