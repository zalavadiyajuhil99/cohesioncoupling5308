package com.dms.meeting;

import com.dms.meeting.interfaces.IViewMeetingsModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertAll;

class ViewMeetingsModelTest {
    IViewMeetingsModel iViewMeetingsModel;
    ViewMeetingsDAOMock viewMeetingsDAOMock;
    @BeforeEach
    void init() {
        viewMeetingsDAOMock = new ViewMeetingsDAOMock();
        iViewMeetingsModel = new ViewMeetingsModel(viewMeetingsDAOMock);
    }
    @Test
    void ViewMeetingsTest() throws SQLException {
        Date today = new Date(System.currentTimeMillis());
        Map<Integer, List> meetingsList = iViewMeetingsModel.meetingsInformation("bhargav",today);
        Map<Integer, List> meetingsList1= iViewMeetingsModel.meetingsInformation("bhargav1",today);
        assertAll(
                () -> Assertions.assertEquals(10000, meetingsList.get(1).get(0)),
                () -> Assertions.assertEquals(2021 - 03 - 24, meetingsList.get(1).get(1)),
                () -> Assertions.assertEquals("3:00 PM", meetingsList.get(1).get(2)),
                () -> Assertions.assertEquals(11111, meetingsList.get(1).get(3)),
                () -> Assertions.assertEquals("No Meetings", meetingsList1.get(1).get(0)),
                () -> Assertions.assertEquals("No Meetings", meetingsList1.get(1).get(0))

        );
    }
}