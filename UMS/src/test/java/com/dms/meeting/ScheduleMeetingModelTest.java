package com.dms.meeting;

import com.dms.meeting.interfaces.IScheduleMeetingModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

class ScheduleMeetingModelTest {
    IScheduleMeetingModel iScheduleMeetingModel;
    ScheduleMeetingDAOMock scheduleMeetingDAOMock;
    @BeforeEach
    void init() {
        scheduleMeetingDAOMock = new ScheduleMeetingDAOMock();
        iScheduleMeetingModel = new ScheduleMeetingModel(scheduleMeetingDAOMock);
    }

    @Test
    void facultyNamesListTest() throws SQLException {
        Map<Integer, List> facultyDetails = iScheduleMeetingModel.facultyNamesList("bhargav");
        assertAll(
                () -> Assertions.assertEquals(5308, facultyDetails.get(1).get(0)),
                () -> Assertions.assertEquals("Advanced Software Development Topics", facultyDetails.get(1).get(1)),
                () -> Assertions.assertEquals(10000, facultyDetails.get(1).get(2))
        );
    }
    @Test
    void validateSlotsTest() throws SQLException {
        Date today = new Date(System.currentTimeMillis());
        assertTrue(iScheduleMeetingModel.validateSlots(10000, today, "3:00 PM", "bhargav"));
    }

}